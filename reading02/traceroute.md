traceroute
==========

Name
--------

	traceroute - print the route packets trace to network host

DESCRIPTION
--------

	traceroute tracks the route packets taken from an IP network on their way to a given host

	The only required parameter is the name or IP address of the destination host

OPTIONS
-------

	-i
		Specifies the interface through which tracerout should send packets. By default, the interface is selected according to the routing table

	-n
		Do not try to map IP addresses to host names when displaying them

	-p /*port*/
		For UPD tracing, specifies the destination port base traceroute will use


RESOURCES
--------

	Information about the project can be found at:

	⟨http://traceroute.sourceforge.net/⟩
