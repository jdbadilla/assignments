scp
==========

Name
--------

	scp - secure copy (remote file copy program)

DESCRIPTION
--------

	scp copies files between hosts on a network. It uses ssh(1) for data transfer, and uses the same authentication and provides the same security as ssh(1). scp will ask for passwords or passphrases if they are needed for authentication

	The options are as follows:

	-1
		forces scp to use protocol 1

	-2
		forces scp to use protocol 2

	-3
		copies between two remote hosts are transferred through the local host. Without this option the data is copied directly between the two remote hosts

	-C 
		Compression enable

	-i 	identity_file
		Selects the file from which the identity (private key) for public key authentication is read. This option directly passed to ssh(1)

	-r
		recursively copy entire directories. Note that scp follows symbolic links encountered in the tree traversal

RESOURCES
--------

	This page is part of the openssh (Portable OpenSSH) project. Information about the project can be found at 

	http://www.openssh.com/portable.html
