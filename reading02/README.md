Reading 02
----------

1. To capture the output of uname to a file called uname.txt:

	uname > uname.txt

2. To find out the IP address associated with the machine, I would:

a.) Create a file from ifconfig:
	
	ifconfig > ifconfig.txt

b.) To look up the IP address in the file using grep marked as 'inet':
	
	grep -i 'inet' ifconfig.txt

3.) To find out the IP address associated with a domain name:

	cd /etc
	less hosts

4.) To find out if a machine is responsive (reachable by the network):

	netstat

5.) I would use the scp command, which securely uses ssh for data transfer.

6.) I would use the ssh command, since it's the most secure. The user must provide his or her identity to the remote machine.

7.) To download a file (named file.txt) from a website (named www.website.com):
	
	curl -o file.txt 'www.website.com/file.txt'

	The -o creates a file with the output rather than outputting it to the screen.

8.) To scan a remote machine to see what ports are open:

	nmap host
