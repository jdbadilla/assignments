ifconfig
==========

Name
--------

	ifconfig - configure a network interface

DESCRIPTION
--------

	ifconfig is used to configure the kernel-resident network interfaces.

	If no arguments are given, ifconfig displays the status of the currently active interfaces.If a sigle interface argument is given, it displays the status of the given interface only.

	If a single -a argument is given, it displays the status of all interfaces even those that are down. Otherwise, it configures an interface.

OPTIONS
-------

	-a
		display all interfaces which are currently available, even if down

	-s
		display a short list (like netstat -i)

	-v
		be more verbose for some error conditions

	interface
		The name of the interface

	up
		This flac causes the interface to be activated. It is implicityly specified if an address is assigned to the interface.

	down
		This flas causes the driver for this interface to be shut down.

	address
		The IP address to be assigned to this interface

RESOURCES
--------

	For more information:

	http://www.openssh.com/portable.html
