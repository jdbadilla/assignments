Reading 02 - Grading
====================

**Score**: 2.5 / 4

Deductions
----------

-0.5 #3 Use `host` or `nslookup`
-0.5 #4 Use `ping`
-0.5 #4 Use `screen` or `tmux`

Comments
--------

- `/etc/hosts` only works for entries stored in there, but not for any domain.
