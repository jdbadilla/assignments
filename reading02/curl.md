curl
==========

Name
--------

	curl - transfer a URL

DESCRIPTION
--------

	curl is a tool to transfer data from or to a server, using one of the supported protocols. The command is designed to work without user interaction.

	curl offers many useful tricks like proxy support, user authentication, FTP upload, HTTP post, SSL connections, cookies, file transfer resume, and more.

	curl is powered by libcurl for all transfer-related features

OPTIONS
-------

	-#
		Make curl display progress as a simple progress bar instead of the standard, more informational, meter.

	--compressed
		(HTTP) Request a compressed response using one of the algorithms curl supports, and save the uncompressed document

	-o
		creates a file with the output rather than outputting it to the screen

	@filename
		This will make curl load data from the given file (including any newlines), URL-encode that data and pass it on in the POST

	--retry <num>
		If a transient error is returned when curl tries to perform a transfer, it will retry this number of times before giving up. Setting the number to 0 makes curl do no retries (which is the default).

	--ssl
		Try to use SSL/TLS for the connection.

	--trace-time
		Assigns a time stamp to each trace or verbose line that curl displays.

	-u <user:password>
		specifiy the user name and password to use for server authentication

AUTHORS/CONTRIBUTORS
--------------------

Daniel Stenberg is the main author.

RESOURCES
--------

	Information about the project can be found at:

	⟨http://curl.haxx.se⟩
