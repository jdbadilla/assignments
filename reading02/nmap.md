nmap
==========

Name
--------

	nmap - Network exploration tool and security / port scanner

DESCRIPTION
-----------

	Nmap (“Network Mapper”) is an open source tool for network exploration and security auditing.

	While Nmap is commonly used for security audits, many systems and network administrators find it useful for routine tasks such as network inventory, managing service upgrade schedules, and monitoring host or service uptime.

TARGET SPECIFICATION
--------------------

	-iL <inputfilename>
		Reads target specifications from inputfilename

	-iR <num hosts>
		For Internet-wide surveys and other research, you may want to choose targets at random. The num hosts argument tells Nmap how many IPs to generate. Undesirable IPs such as those in certain private, multicast, or unallocated address ranges are automatically skipped.

	-o
		creates a file with the output rather than outputting it to the screen

	@filename
		This will make curl load data from the given file (including any newlines), URL-encode that data and pass it on in the POST

	--retry <num>
		If a transient error is returned when curl tries to perform a transfer, it will retry this number of times before giving up. Setting the number to 0 makes curl do no retries (which is the default).

	--ssl
		Try to use SSL/TLS for the connection.

	--trace-time
		Assigns a time stamp to each trace or verbose line that curl displays.

	-u <user:password>
		specifiy the user name and password to use for server authentication

HOST DISCOVERY
--------------

	-sL (List scan)
		The list scan is a degenerate form of host discovery that simply lists each host of the network(s) specified, without sending any packets to the target hosts

OUTPUT
------

	-oN <filespec> (normal output)
		Requests that normal output be directed to the given filename

AUTHORS
-------

	Gordon "Fyodor" Lyon <fyodor@nmap.org>

	<http://insecure.org>
