Homework 08
===========

1. I scanned the filesystem by traversing through the list of objects in the directory using the os.listdir('dir_name') command. Various try and except blocks were located within this for loop.

2. I loaded the rules by using the file() command, where my two arguments were the new set of rules and the symbol for read or write permissions. I then obtained the rules from the yaml.load() command. I stored these rules onto a new blank list.

3. I used arrays to help determine changes to files. The logic I used was within a for loop reading the elements in the directory and various try and except blocks populated the for loop to ensure errors could be captured and handled gracefully.

4. I executed each action by going through the loop and verifying that the process identification was that of a child process, and then used execvp() command in which I called the main command as the first argument and a list of arguments as the second.

5. The issue with catch invalidation is when a file is deleted while it's being modified and upon looping around the file is not there, thus causing a memory allocation-like error. Busy waiting is when the time you lose from looping from a bunch of directories causes the code to be inefficient.