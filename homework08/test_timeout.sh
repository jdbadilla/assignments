#!/bin/bash

filename="timeout.py"

# 1.) Verify that timeout.py is executable

if ! [[ -x "$filename" ]]
then
	echo "File '$filename' is not executable or found!"
	exit 1
fi

# 2.) Verify that timeout.py has python2.7 in the she-bang:

if ! egrep -q '.*python2.7$' $filename; then
	echo "File '$filename' does not contain the python2.7 she-bang!"
	exit 1
fi

# 3.) Verify that timeout.py prints something reasonable to STDERR when the -h flag is set:

./timeout.py -h > usage.txt 2>&1

if ! egrep -q '[-t] SECONDS' usage.txt; then
    echo "timeout -h flag test failed!"
    exit 1
fi

if ! egrep -q '[-v]' usage.txt; then
    echo "timeout -h flag test failed!"
    exit 1
fi

rm usage.txt

# 4.) Verify that timeout.py exits with SUCCESS when executing:

if ! diff -u <(echo "Success") \
             <(./timeout.py -t 5 sleep 1 && echo 'Success'); then
    echo "timeout SUCCESS (timeout duration) test failed!"
    exit 1
fi

if ! diff -u <(echo "Success") \
             <(./timeout.py -t 5 sleep 2 && echo 'Success'); then
    echo "timeout SUCCESS (timeout duration) test failed!"
    exit 1
fi

if ! diff -u <(echo "Success") \
             <(./timeout.py -t 5 sleep 3 && echo 'Success'); then
    echo "timeout SUCCESS (timeout duration) test failed!"
    exit 1
fi

if ! diff -u <(echo "Success") \
             <(./timeout.py -t 5 sleep 4 && echo 'Success'); then
    echo "timeout SUCCESS (timeout duration) test failed!"
    exit 1
fi

# 5.) Verify that timeout.py exits with FAILURE when executing:

if ! diff -u <(echo "Failure") \
             <(./timeout.py -t 1 sleep 2 || echo 'Failure'); then
    echo "timeout FAILURE (timeout duration) test failed!"
    exit 1
fi

if ! diff -u <(echo "Failure") \
             <(./timeout.py -t 1 sleep 3 || echo 'Failure'); then
    echo "timeout FAILURE (timeout duration) test failed!"
    exit 1
fi

if ! diff -u <(echo "Failure") \
             <(./timeout.py -t 1 sleep 4 || echo 'Failure'); then
    echo "timeout FAILURE (timeout duration) test failed!"
    exit 1
fi

if ! diff -u <(echo "Failure") \
             <(./timeout.py -t 1 sleep 5 || echo 'Failure'); then
    echo "timeout FAILURE (timeout duration) test failed!"
    exit 1
fi

# 6.) Verify that timeout.py prints something reasonable to STDERR when the -v flag is set.

./timeout.py -v sleep 3 > verbose.txt 2>&1

if ! egrep -q 'Exec.*' verbose.txt; then
	echo "'$filename' -v flag test failed!"
	exit 1
fi

if ! egrep -q 'Forking...' verbose.txt; then
	echo "'$filename' -v flag test failed!"
	exit 1
fi

if ! egrep -q 'Enabling Alarm...' verbose.txt; then
	echo "'$filename' -v flag test failed!"
	exit 1
fi

if ! egrep -q 'Waiting...' verbose.txt; then
	echo "'$filename' -v flag test failed!"
	exit 1
fi

if ! egrep -q 'Disabling Alarm...' verbose.txt; then
	echo "'$filename' -v flag test failed!"
fi

rm verbose.txt

echo "'$filename' test successful!"