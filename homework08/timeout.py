#!/usr/bin/env python2.7

import getopt
import sys
import os
import signal

# Global Variables

SET_TIME=False
SECONDS=10	# timeout is set to 10 seconds by default
VERBOSE=False
COMMAND=''
ARGS=[COMMAND]
status=0

# usage function

def usage(usage_status=0):
	print '''Usage: timeout.py [-v] [-t SECONDS] command...

Options:

    -t SECONDS  Timeout duration before killing command (default is 10 seconds)
    -v          Display verbose debugging output'''
	sys.exit(usage_status)

# signal handler

def sig_handler(signum, frame):
	if VERBOSE:
		global SECONDS, pid, status
		print 'Alarm triggered after {} seconds!'.format(SECONDS)
		print 'Killing PID {}...'.format(pid)
		print 'Disabling alarm...'
		print 'Process {0} terminated with exit status {1}'.format(pid,signum)
	sys.exit(int(signum))

# getopt implementation

try:
	options, remainder = getopt.getopt(sys.argv[1:],'t:vh')
except getopt.GetoptError as e:
	print e
	usage(1)

for opt, arg in options:
	if opt == '-t':
		SET_TIME = True
		SECONDS = int(arg)
	elif opt == '-v':
		VERBOSE = True
	elif opt == '-h':
		usage(1)
	else:
		usage(1)

# COMMAND definition

if SET_TIME and VERBOSE:
	COMMAND = str(sys.argv[4])
	tmp = 5
	while len(sys.argv) > tmp:
		ARGS.append(str(sys.argv[tmp]))
		tmp += 1
elif SET_TIME:
	COMMAND = str(sys.argv[3])
	tmp = 4
	while len(sys.argv) > tmp:
		ARGS.append(str(sys.argv[tmp]))
		tmp += 1
elif VERBOSE:
	COMMAND = str(sys.argv[2])
	tmp = 3
	while len(sys.argv) > tmp:
		ARGS.append(str(sys.argv[tmp]))
		tmp += 1
else:
	COMMAND = str(sys.argv[1])
	tmp = 2
	while len(sys.argv) > tmp:
		ARGS.append(str(sys.argv[tmp]))
		tmp += 1

ARGS[0] = COMMAND

# debug function

def debug(message, *args):
	if VERBOSE:
		print message.format(*args)

# execute command and set signal

signal.signal(signal.SIGALRM, sig_handler)

debug('Executing "{}" for at most {} seconds...', COMMAND, SECONDS)
try:
	debug('Forking...','')
	debug('Enabling Alarm...','')
	pid = os.fork()
	signal.alarm(SECONDS)
except OSError as e:
	print >> sys.stderr, 'Unable to fork: {}'.format(e)
	sys.exit(1)	
if pid == 0:	# Child
	try:
		debug('Execing...','')
		os.execvp(COMMAND, ARGS)
	except OSError as e:
		print >> sys.stderr, 'Unable to execute: {}'.format(e)
else:		# Parent
	try:
		debug('Waiting...','')
		pid, status = os.wait()
	except OSError as e:
		os.kill(pid,signal.SIGTERM)
		pid, status = os.wait()

debug('Disabling Alarm...','')
debug('Process {} terminated with exit status {}',pid, status)
sys.exit(int(status))


