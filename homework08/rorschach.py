#!/usr/bin/env python2.7

import os
import time
import getopt
import sys
import re
import glob
import shlex
import signal
import errno
import yaml

# Global Variables

RULES = 'rorschach.yml'
SECONDS = 2
VERBOSE = False
number_flags = 0
NEW = False
MODIFY = False
dirs = []

def debug(message, *args):
	if VERBOSE:
		print message.format(*args)

def usage(status=0):
	print '''usage: rorschach.py [-r RULES -t SECONDS] DIRECTORIES ...

	Options

	-r	RULES	Path to rules file (defualt is .rorschach.yml)
	-t	SECONDS	Time Between scans (default is 2 seconds)
	-v			Display verbose debugging output'''.format(os.path.basename(sys.argv[0]))
	sys.exit(status)
				
try:
	options, remainder = getopt.getopt(sys.argv[1:],'r:t:vh')	# r and t contain arguments
except getopt.GetoptError as e:
	print e
	usage(1)
							
for opt, arg in options:
	if opt == '-r':
		RULES = arg
		number_flags += 2
	elif opt == '-t':
		SECONDS = arg
		number_flags += 2
	elif opt == '-v':
		VERBOSE = True
		number_flags += 1
	elif opt == '-h':
		usage(1)
	else:
		usage(1)

#check for proper input
if len(sys.argv) < number_flags + 2:	# verify directory input
	print 'Enter a directory!'
	usage(1)

#read in directories
for item in sys.argv[number_flags+1:]:	# read from the argument following the flags onward
	dirs.append(item)

#load rules
stream = file(RULES, 'r')
RULES = yaml.load(stream)
actions = []
patterns = []
for rule in RULES:
	actions.append(rule['action'])
	patterns.append(rule['pattern'])

arguments = []
for action in actions:
	arguments.append(shlex.split(action))

before_time = []
before_name = []
for d in dirs:
	for file in os.listdir(d):
		before_time.append(os.stat(d+'/'+file).st_mtime)
		before_name.append(d+'/'+file)

time.sleep(int(SECONDS))	
ix = 0
ic = -1
for d in dirs:
	for file in os.listdir(d):
		fullpath = d + '/' + file
		if fullpath not in before_name:
			NEW = True
		elif before_time[ix] != os.stat(d+'/'+file).st_mtime:
			MODIFY = True
		ic = -1
		for pattern in patterns:
			ic = ic + 1
			try:
				regEx = re.search(pattern,file)
			except:
				regEx = 0
			try:
				globEx = glob.glob(d+'/'+pattern)
			except:
				globEx = 0
			if (regEx or globEx) and (MODIFY or NEW):
				try:
					debug('Forking...','')
					pid = os.fork()
				
					if pid == 0:    # Child
						try:
							debug('Execing...','')
							do = arguments[ic][1].format(name=file, path=fullpath)
							arguments[ic][1] = do
							os.execvp(arguments[ic][0], arguments[ic][:2])
						except OSError as e:
							debug('Unable to exec: {}', e)
					else:           # Parent
						debug('Waiting...','')
						pid, status = os.wait()
				except OSError as e:# Error
					debug('Unable to fork {}',e)


		if not NEW:
			ix = ix+1
		MODIFY = False
		NEW = False
