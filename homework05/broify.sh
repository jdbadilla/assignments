#!/bin/sh

DELIM='#'
Woption=0

usage()
{
	echo "USAGE: ./broify.sh [options]"
	echo "-d DELIM  	Use this as the comment delimiter."
	echo "-W 			Don't strip empty lines"
	exit 1
}

while getopts d:W name; do
	case $name in
		d) DELIM=$OPTARG;;
		W) Woption=1;;
		*) usage
	esac
done

shift $(($OPTIND-1))

remove()
{
	if [ $Woption -eq 0 ]; then
		sed '/^s*$/d'
	else
		cat
	fi
}

sed "s|$DELIM.*||" | sed "s|[ \t]*$||" | remove

