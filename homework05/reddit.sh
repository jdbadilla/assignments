#!/bin/sh

: ${FLAG:="0"}
num=10

while getopts rsn:N name; do
	case $name in
		r)
			FLAG="shuf"
			;;
		s)
			FLAG="sort"
			;;
		n | N)
			if [ -z $OPTARG ]; then
				num=10
			else
				num=$OPTARG
			fi
			;;
	esac
done

shift "$((OPTIND-1))"

for i in $@; do
	eval argument=\$i
	curl -s http://www.reddit.com/r/$argument/.json | python -m json.tool > tempfile.txt
	grep 'url' tempfile.txt | sed -e 's/^.*: "//g' |  sed -e 's/".*$//g' | grep "$argument" | head -n $num > targetfile.txt
	if [ $FLAG != 0 ]; then
		$FLAG targetfile.txt
	else
		cat targetfile.txt
	fi
	echo
done
