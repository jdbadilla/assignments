Homework 05
===========

Activity 01
-----------

1. I constructed a source set by creating two subsets within it dealing with both the lower case and upper case letters of the alphabet. This was done with the 'echo {A..Z}/{a..z}', which creates a string of the characters of the alphabet separated by whitespaces in between. The sed command was used to parse this text and remove the blank spaces.

2. I constructed the target set by using 'sed' to parse the alphabet string and remove the first n specified number of characters and concatinating that to a another parsed string of the alphabet with 26-n characters removed from its end. I constructed a cipher string for both upper and lower case letters.

3. I used both of these sets, along with the help of the 'tr' command, which allows the translation, editing and deletion of characters in text, to perform the necessary number of rotations and encrypt the text. I did this by associating the upper case letters in the received string with the upper case alphabet string and the lower case letters with the lower case alphabet string (the source set), and then associating both of these sets with their respective cased ciphers.


Activity 02
-----------

1. I used a variation of grep and sed commands to extract the relevant portion of the data, all of which was saved in a document called targetfile. The grep command deleted the url and the sed commands got rid of the other irrelevant punctiation, spacing, etc.

2. To handle the options, I used the command getopts within a while loop. To sort the http addresses, I simply used the sort command and associated that with option s. To randomize the order of the http addresses, I simply used the shuf command (or gshuf on Mac). Without arguments, the program just displayes the http addresses in the order obtained. Within getops option n, 10 was declared to be the default number of links shown.

3. The command line options complemented the operations above. As said, with the use of the getopts command within the while loop, it was much easier to assign the sort and shuf flags to the command retrieving the links. It was also good in terms of code organization. An if statement at the end is used to determine a flag was specified and if so assigns it to the specified command within the targetfile.txt file.


Activity 03
-----------

1. The very last line of the script removes comments. The delimeter of comments was changed by assigning the # sign to the DELIM variable, calling every line starting with the # sign, removing the # sign from these lines using the 'sed' command to parse text and replacing it with the specified delimiter (upon using the -d flag).

2. The remove function within the shell script removes all empty lines. An if statement was used to aid in the operation, containing a simple 'sed' command with the /d option.

3. Like in Activity 02, a while loop with a getopts command was used to accept command line options. By setting the Woption to 1 upon calling the -W flag, the program prevents the stripping of empty lines. When the -d flag is used, the delimeter may be specified to be the option argument passed by the user. The final operations are affected by replacing the # delimiter with the new character or string specified by the user upon calling the -d flag.