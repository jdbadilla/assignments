#!/bin/bash

newvalue=$(($1 % 26))

UpperAlphabet=$(echo {A..Z} | sed 's/ //g')	#creates UpperAlphabet string and deletes whitespaces in between
LowerAlphabet=$(echo {a..z} | sed 's/ //g')

UpperCipher=$(echo "$UpperAlphabet" | sed -e 's/^.\{'${newvalue:-13}'\}//g')$(echo "$UpperAlphabet" | sed -e 's/.\{'$( expr 26 - ${newvalue:-13} )'\}$//g' ) #moves 23 steps to the right
LowerCipher=$(echo "$LowerAlphabet" | sed -e 's/^.\{'${newvalue:-13}'\}//g')$(echo "$LowerAlphabet" | sed -e 's/.\{'$( expr 26 - ${newvalue:-13} )'\}$//g' )

tr '[:upper:]' "$UpperAlphabet" | tr '[:lower:]' "$LowerAlphabet" | tr "$UpperAlphabet" "$UpperCipher" | tr "$LowerAlphabet" "$LowerCipher"