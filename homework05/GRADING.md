uhomework 05 - Grading
======================

**Score**: 14 / 15

Deductions
----------

caesar.sh:

-0.25 didn't handle -h flag

broify.sh:

-0.5 didn't pass one test (C++)
-0.25 didn't give sed patterns

Comments
--------
