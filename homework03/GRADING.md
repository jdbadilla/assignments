homework 03 - Grading
======================

**Score**: 12.5 / 15

Deductions
----------

-1.5 for not using variables,patterns, or auto variables in rules in 1st makefile
-0.5 for no settings and not using variables in 2nd makefile
-0.5 for not using strace in courier

Comments
--------
