Homework 03
===========

Activity 01
-----------

1. By using the ls -lh command, I was able to see the size of the files. 

	a. The libgcd.a file is 2.5 Kilobytes while the libgcd.so is only 4.4 Kilobytes. The reason the libgcd.so file is bigger is because shared libraries require more space in order to be used by several a.out files simultaneously while executing.

	b. The gcd-static file is 8.9 Kilobytes while the gcd-dynamic file is 8.7 Kilobytes. Static executables are larger in size because external programs are built into them. In dynamic linking, only one copy of shared library is kept in memory, thereby saving disk space.

2. The gcd-static executable is linked to the libgcd.so and /usr/lib/libSystem.B.dylib libraries. The gcd-dynamic is linked exclusively to the /usr/lib/libSystem.B.dylib library. I found out by using the Mac OSX equivalent of the ldd command: otool -L (prints shared library dependencies)

3. Running the gcd-dynamic application worked. Since the dynamic executable is linked to the static as well as the shared library, it has access to the shared objects at runtime. On a Mac, the environment variable we would set is LD_LIBRARY_PATH. To set it, use the command export like this:

		export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/libSystem.B.dylib

4. Although the static linking model is simple to implement and use, it is its only advantage over dynamic linking. By statically linking a program, the resulting executable file will contain all the data that the program may ever need. This may require a lot of memory space. The dynamic linking model offers code sharing, automatic updates (since a new version of a dynamically linked library will automatically supercede the preexisting one). It also provides an extra layer of security (several linkage units are safer than one). This is why, if I were to create an application I would want it to produce a dynamic executable by default.


Activity 02
-----------

1. To download and extract the is_palindrome.tar.gz file I used the following commands:

	curl -O http://www3.nd.edu/~pbui/teaching/cse.20189.sp16/static/tar/is_palindrome.tar.gz
	tar xvzf is_palindrome.tar.gz

2. I used the -g flag with gcc to include debugging symbols in the is_palindrome executable. Including these symbols increases the size of the executable, since the executable will contain more information to make it "debugging-friendly". For evidence, I have compiled the modified is_palindrome.c file with and without the -g flag and compared at their respective file sizes:

	cc -c is_palindrome.c -o is_palindrome_no_debugging.o
	gcc -g -c is_palindrome.c -i is_palindrome_yes_debugging.o
    ls -lh *.o

    OUTPUT:

    -rw-r--r--  1 jdbadilla  staff   1.8K Feb  5 21:30 is_palindrome_no_debugging.o
    -rw-r--r--  1 jdbadilla  staff   4.0K Feb  5 21:30 is_palindrome_yes_debugging.o

3. To diagnose the program I used the gdb command:
	
	gdb ./is_palindrome
	(gdb) run
	(gdb) f 1
	(gdb) info f
	(gdb) bt
	(gdb) f 2
	(gdb) p buffer

	I initially ran the code and then used to bt to locate the line with the error.

	char buffer[BUFSIZ] = "";

	To locate the invalid memory address I used:

	valgrind ./is_palindrome < is_palindrome.input

	This helped me identify that there was an error within lines 10 and 27. The error occured because the function was reading the null terminator at the end of the string. The back pointer was one longer than necessary. I then implemented the following changes:

	const char *back = s + strlen(s) - 1;

	Also, the sanitize_string function failed to add the null terminator to the *writer pointer:

	*writer = '\0';

	To stop any memory leaks from occuring, you must free up the memory of the *sanitized pointer used after the printf function in the main function:

	free(sanitized);

4. The memory leak was definitely the hardest bug to fix. Not only was it difficult to locate because I had to carefully look between lines 10 and 27, but it also required me to keep track of which assignments and functions were taking the null terminator into account. Finally, I had to verify that the sanitized pointer was freed to make sure there was no memory leaks.


Activity 03
-----------

1. Contacting the COURIER:

	$ /afs/nd.edu/user15/pbui/pub/bin/COURIER

	He had the following message:
	"Hmm...you sure you put the package in the right place?"

2. Locating the package:

	$ strings COURIER

	This shows that the COURIER is looking for a file in /tmp/ called "%s.deaddrop". The %s indicates that the user may input his/her netID in its stead.

3. Creating the file:

	$ cd /tmp/
	$ touch jbadilla.deaddrop
	$ /afs/nd.edu/user15/pbui/pub/bin/COURIER

	Upon visiting the COURIER this time, he said:
	"Whoa whoa...you can't give everyone access to the package! Lock it down!"

4. Locking down the package:

	$ cd /tmp/
	$ chmod 700 jbadilla.deaddrop
	$ /afs/nd.edu/user15/pbui/pub/bin/COURIER

	It then said:
	"What are you trying to pull here? The package is the wrong size!"
	This means the file is empty and must have some content.

5. Writing to the file:

	$ cd /tmp/
	$ nano jbadilla.deaddrop
	*input any text in file* (in my case, I typed deaddrop)
	/afs/nd.edu/user15/pbui/pub/bin/COURIER

	It finally said:
	"Well, everything looks good...I'm not sure what 'deaddrop' means, but I'll pass it on."