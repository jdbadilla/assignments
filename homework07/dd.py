#!/usr/bin/env python

import getopt
import sys
import os

# Usage Function

def usage(status=0):
	print '''Usage: dd.py options...

	Options:

	if=FILE     Read from FILE instead of stdin
	of=FILE     Write to FILE instead of stdout

	count=N     Copy only N input blocks
	bs=BYTES    Read and write up to BYTES bytes at a time

	seek=N      Skip N obs-sized blocks at start of output
	skip=N      Skip N ibs-sized blocks at start of input'''.format(os.path.basename(sys.argv[0]))
	sys.exit(status)

# Global variables

R_FILE=False
W_FILE=False
COUNT=False
BYTES=False
SEEK=False
SKIP=False

count_blocks=4096	# default values
block_size=512
blocks_seek=0
blocks_skip=0
bytes_skip=0
bytes_seek=0
infile=0
outfile=1

# obtain command line option and arguments

commandLine=[]
error=1

for option in sys.argv[1:]:		# define option arguments
	for char in option:
		if char == '=':
			error = 0
	if error == 1:
		option += '='
	commandLine.append(option.split('=',1))

for opt, arg in commandLine:
	if opt == 'if':
		R_FILE = True
		file_input = arg
	elif opt == 'of':
		W_FILE = True
		file_output = arg
	elif opt == 'count':
		COUNT = True
		count_blocks = arg
	elif opt == 'bs':
		BYTES = True
		block_size = arg
	elif opt == 'seek':
		SEEK = True
		blocks_seek = arg
	elif opt == 'skip':
		SKIP = True
		blocks_skip = arg
	else:
		usage(1)

number_bytes = int(count_blocks) * int(block_size)

if SKIP:
	bytes_skip = int(block_size) * int(blocks_skip)

if SEEK:
	bytes_seek = int(block_size) * int(blocks_seek)

if R_FILE:
	infile = os.open(file_input,os.O_RDONLY)
	os.lseek(infile,int(bytes_skip),0)

if W_FILE:
	outfile = os.open(file_output,os.O_WRONLY|os.O_CREAT)
	os.lseek(outfile,int(bytes_seek),0)

os.write(outfile, os.read(infile, int(number_bytes)))
