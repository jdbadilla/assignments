#!/usr/bin/env python

import getopt
import sys
import os
import re
import time
import fnmatch
from stat import *

# Usage Function

def usage(status=0):
	print '''Usage: find.py directory [options]...

	Options:

	-type [f|d]     File is of type f for regular file or d for directory

	-executable     File is executable and directories are searchable to user
	-readable       File readable to user
	-writable       File is writable to user

	-empty          File or directory is empty

	-name  pattern  Base of file name matches shell pattern
	-path  pattern  Path of file matches shell pattern
	-regex pattern  Path of file matches regular expression

	-perm  mode     File's permission bits are exactly mode (octal)
	-newer file     File was modified more recently than file

	-uid   n        File's numeric user ID is n
	-gid   n        File's numeric group ID is n'''.format(os.path.basename(sys.argv[0]))
	sys.exit(status)

if len(sys.argv) == 1:
	usage(1)

if not os.path.isdir(sys.argv[1]):
	sys.exit("ERROR: No such directory!")

# Global variables

TYPE=False
F_FLAG=False
D_FLAG=False
EXE=False
READ=False
WRITE=False
EMPTY=False
NAME=False
name_pattern=''
PATH=False
path_pattern=''
REGEX=False
regex_pattern=''
PERM=False
octal=000
NEWER=False
newer_file=''
UID=False
uid=0
GID=False
gid=0
isFILE=False
isDIR=False

rootDir = str(sys.argv[1])

arg=None

# 'include' function

def include(fpath):
	status = True
	
	execute = os.stat
	try:
		os.stat(fpath)
	except OSError, e:
		execute = os.lstat

	if EXE and not os.access(fpath, os.X_OK):
		status = False
		return status
	if WRITE and not os.access(fpath, os.W_OK):
		status = False
		return status
	if READ and not os.access(fpath, os.R_OK):
		status = False
		return status
	if EMPTY:
		tmp = execute(fpath).st_mode

		if S_ISDIR(tmp):
			try:
				if os.listdir(fpath):
					status = False
					return status
			except:
				status = False
				return status
		if S_ISLNK(tmp):
			if execute == os.lstat:	#if link is broken
				status = False
				return status
		if S_ISREG(tmp):
			size = execute(fpath).st_size
			if size != 0:
				status = False
				return status
	if NAME:
		namestr = fpath.split('/')[-1]
		if not fnmatch.fnmatch(namestr, name_pattern):
			status = False
			return status

	if PATH:
		if not fnmatch.fnmatch(fpath, path_pattern):
			status = False
			return status

	if REGEX:
		if not re.match(regex_pattern, fpath):
			status = False
			return status
	if PERM:
		mod = oct(execute(fpath).st_mode & 0777)
		mod = mod[-3:]
		if mod != octal:
			status = False
			return status
	if NEWER:
		newer_file_time = os.stat(newer_file).st_mtime
		fpath_time = execute(fpath).st_mtime
		if not fpath_time > newer_file_time:
			status = False
			return status
	if UID:
		fpath_uid = execute(fpath).st_uid
		if int(uid) != int(fpath_uid):
			status = False
			return status
	if GID:
		fpath_gid = execute(fpath).st_gid
		if int(gid) != int(fpath_gid):
			status = False
			return status
	if TYPE:
		tmp = execute(fpath).st_mode
		if F_FLAG:
			if not S_ISREG(tmp):
				status = False
				return status
		elif D_FLAG:
			if not S_ISDIR(tmp):
				status = False
				return status

	return status

commandLine=[]

for option in sys.argv[2:]:
	isOpt = False
	for char in option:
		if char == '-':
			isOpt = True
		elif char == ']':
			isOpt = False
		else:
			continue
	if isOpt:
		if option in ('-type','-executable','-readable','-writable','-empty','-name','-path','-regex','-perm','-newer','-uid','-gid'):
			if option in ('-executable','-readable','-writable','-empty'):
				commandLine.append(option)
				commandLine.append('*')
			else:
				commandLine.append(option)
		else:
			usage(1)
	else:
		commandLine.append(option)

for index, opt in enumerate(commandLine):
	isOpt = False
	for char in opt:
		if char == '-':
			isOpt = True
		elif char == ']':
			isOpt = False
		else:
			continue
	if isOpt:
		if opt == "-type":
			arg = commandLine[index+1]
			TYPE = True
			if arg == 'f':
				F_FLAG = True
			elif arg == 'd':
				D_FLAG = True
			else:
				sys.exit("ERROR:'-type' option requires 'f' argument for file or 'd' for directory!")
		elif opt == "-executable":
			EXE = True
		elif opt == "-readable":
			READ = True
		elif opt == "-writable":
			WRITE = True
		elif opt == "-empty":
			EMPTY = True
		elif opt == "-name":
			NAME = True
			arg = commandLine[index+1]
			name_pattern = arg
		elif opt == "-path":
			PATH = True
			arg = commandLine[index+1]
			path_pattern = arg
		elif opt == "-regex":
			REGEX = True
			arg = commandLine[index+1]
			regex_pattern = re.compile(arg)
		elif opt == "-perm":
			PERM = True
			arg = commandLine[index+1]
			octal = arg
		elif opt == "-newer":
			NEWER = True
			arg = commandLine[index+1]
			newer_file = arg
		elif opt == "-uid":
			UID = True
			arg = commandLine[index+1]
			uid = arg
		elif opt == "-gid":
			GID = True
			arg = commandLine[index+1]
			gid = arg
		else:
			usage(1)
	elif isOpt and opt not in ('-type','-executable','-readable','-writable','-empty','-name','-path','-regex','-perm','-newer','-uid','-gid'):
		usage(1)		
	elif not isOpt and opt != '*':
		if commandLine[index-1] not in ('-type','-name','-path','-regex','-perm','-newer','-uid','-gid'):
			usage(1)
		else:
			continue			# make a case for arguments without a hyphen


# list fileList and directories in specified directory

output=[]

if include(rootDir):
    output.append(rootDir)
for dirName, subdirList, fileList in os.walk(rootDir, topdown=True, onerror=None, followlinks=True):
    for name in subdirList + fileList:
        filepath = os.path.join(dirName, name)
        if include (filepath):
            output.append(filepath)

# display output

for line in sorted(output):
	print line

