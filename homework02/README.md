Homework 02
===========

1. # Create the folder /tmp/jbadilla-workspace on two different student machines: the source remote machine is student03

   ssh jbadilla@student03.cse.nd.edu
   mkdir /tmp/jbadilla-workspace
   exit

 	# Generate a 10MB file full of random data

 	dd if=/dev/urandom of=file.txt bs=10000000 count=1

 	# Create 10 hard links to 10MB file

 	ln file.txt file1.txt
 	ln file.txt file2.txt
 	...
 	...
 	ln file.txt file9.txt
 	ln file.txt file10.txt

   # Create workspace on targe machine: the target machine is student00.cse

 	ssh jbadilla@student00.cse.nd.edu
 	mkdir /tmp/jbadilla-workspace
   	exit

2. # Upon running the du command in the /tmp/jbadilla-workspace/ folder of the source machine, the total disk usage is 11 MB.
   # This is surprising since the folder contains a total of eleven 10 M files, meaning it should be 110 M.

  	du -h

3. # Performing the du command on the destination folder (with the -h option for human redability) showed that the disk usage
   # of the destination folder was 111 M, which is close to what I had predicted earlier regarding the source folder. The
   #

4. # Transfer data files using scp:
	
	ssh jbadilla@student03.cse.nd.edu
	scp -r /tmp/jbadilla-workspace/ jbadilla@student00.cse.nd.edu:/tmp/jbadilla-workspace
	exit

   # Transfer data files using sftp:
   # Since the original file and the 10 hardlinks all had a file name that started with "file...", I used the wildcard
   # character '*' to choose to obtain all 11 files easily with one command:

   ssh jbadilla@student00.cse.nd.edu	# log into the target machine
   cd /tmp/jbadilla-workspace/			# cd into the desired folder to get the files
   sftp jbadilla@student03.cse.nd.edu	# use sftp to log into the source remote machine
   get /tmp/jbadilla-workspace/file*	# 'get' all the files
   exit

   # Transfer data files using rsync:

   ssh jbadilla@student03.cse.nd.edu	# log into the source machine
   rsync -r -e ssh /tmp/jbadilla-workspace/ jbadilla@student00.cse.nd.edu:/tmp/jbadilla-workspace
   exit

5. # rsync is faster than scp and sftp if the target already contains some of the source files, since rsync only copies the difference that has not been copied
   # rsync is therefore better for a large number of small files

6. # I like scp and rsync much more than sftp for the following reasons:

	# 1. It is its own program. I prefer the least amount of steps.
	# 2. The scp and rsync commands just require one line of code specifying the source and the target
	#	(You will be prompted for authentication)
