Homework 02 - Grading
=====================

**Score**: 14 / 15

Deductions
----------

-0.5 (2)
-0.5 (3)

Comments
--------

Activity 1:

(2) Is this number surprising? Why or why not?
No, because the hard links just store references to existing inodes and do not copy the contents of the file.

(3) Is this number different from the one above? Why or why not?

Yes, the number is different.  This is because when we transfer the files, we are creating a new file and making a copy (rather than using hard links).
