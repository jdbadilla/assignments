#!/usr/bin/env python

import getopt
import sys
import os 	#used to include usage function

# Global variables

number_lines = 10
toplines = []
ix = 0	#initialize counter
isFlag = False

# usage function

def usage(status=0):
	print '''usage: head.py [-n NUM] files ...

    -n NUM  print the first NUM lines instead of the first 10'''.format(os.path.basename(sys.argv[0]))
	sys.exit(status)

# getopts implementation

try:
	options, remainder = getopt.getopt(sys.argv[1:],'n:')
except getopt.GetoptError as e:
	print e
	usage(1)

for opt, arg in options:
	if opt == '-n':
		number_lines = arg
		isFlag = True
	else:
		usage(1)

# read from standard input

if len(sys.argv) == 1:
	sys.argv.append('-')
if isFlag and len(sys.argv) == 3:
	sys.argv.append('-')

for path in sys.argv[-1]:
	if path == '-':
		stream = sys.stdin
	else:
		stream = open(sys.argv[-1],"r")

# display results

for line in stream:
	print line.strip()
	ix += 1
	if ix == int(number_lines):
		sys.exit
		break

stream.close()