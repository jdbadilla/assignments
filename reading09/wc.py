#!/usr/bin/env python

import getopt
import sys
import os 	#used to include usage function

# Global Variables

CHAR = False
LINE = False
WORD = False
number_flags = 0

# Usage function

def usage(status=0):
	print '''usage: wc.py [-c -l -w] files ...

    -c      print the byte/character counts
    -l      print the newline counts
    -w      print the word counts'''.format(os.path.basename(sys.argv[0]))
	sys.exit(status)

# getopts implementation

try:
	options, remainder = getopt.getopt(sys.argv[1:],'clw')
except getopt.GetoptError as e:
	print e
	usage(1)

for opt, arg in options:
	if opt == '-c':
		CHAR = True
		number_flags += 1
	elif opt == '-l':
		LINE = True
		number_flags += 1
	elif opt == '-w':
		WORD = True 
		number_flags += 1
	else:
		usage(1)


# read from standard input

if number_flags == 0:
	sys.exit("ERROR: No flag was provided! (Note: List each flag with its own hyphen and adequate spacing!)")
elif number_flags == 1 and len(sys.argv) == 2:
	sys.argv.append('-')
elif number_flags == 2 and len(sys.argv) == 3:
	sys.argv.append('-')
elif number_flags == 3 and len(sys.argv) == 4:
	sys.argv.append('-')

for path in sys.argv[-1]:
	if path == '-':
		stream = sys.stdin
	else:
		stream = open(sys.argv[-1],"r")

# display results

charN=0
lineN=0
wordN=0

for line in stream:
	for char in line:
		charN += 1
		if char.isspace():
			wordN += 1
	lineN += 1

if CHAR == True:
	print ('bytes: ' + str(charN))
if LINE == True:
	print ('lines: ' + str(lineN))
if WORD == True:
	print ('words: ' + str(wordN))

stream.close()