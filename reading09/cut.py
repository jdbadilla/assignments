#!/usr/bin/env python

import getopt
import sys
import os 	#used to include usage function

# Global variables

DELIM = '\t'
FIELDS = []
F_FLAG = False
number_flags = 0

# Usage function

def usage(status=0):
	print '''usage: wc.py [-d DELIM -f FIELDS] files ...

    -d DELIM  use DELIM instead of TAB for field delimiter
    -f FIELDS select only these FIELDS'''.format(os.path.basename(sys.argv[0]))
	sys.exit(status)

# getopts implementation

try:
	options, remainder = getopt.getopt(sys.argv[1:],'d:f:')
except getopt.GetoptError as e:
	print e
	usage(1)

for opt, arg in options:
	if opt == '-d':
		DELIM = arg
		number_flags += 1
	elif opt == '-f':
		F_FLAG = True
		number_flags += 1
		for char in arg:
			if char.isdigit():
				intchar = int(char)
				FIELDS.append(intchar-1)
			else:
				continue
	else:
		usage(1)

if F_FLAG == False:
	sys.exit("ERROR: Necessary '-f' flag not provided!")

# read from standard input

if number_flags == 1 and len(sys.argv) == 3:
	sys.argv.append('-')
if number_flags == 2 and len(sys.argv) == 5:
	sys.argv.append('-')

for path in sys.argv[-1]:
	if path == '-':
		stream = sys.stdin
	else:
		stream = open(sys.argv[-1],"r")

# display results

for line in stream:
	occurences = 0		# occurences of delimiter
	for char in line:		
		if char == DELIM:
			occurences += 1
	for ix in range(0,len(FIELDS)):
		if int(FIELDS[ix]) >= occurences + 1:
			break
		else:
			sys.stdout.write(line.split(DELIM)[int(FIELDS[ix])])
		if ix < len(FIELDS) - 1:
			sys.stdout.write(DELIM)
	print '\n'
stream.close()





