#!/usr/bin/env python

import getopt
import sys
import os 	#used to include usage function

# Global Variables

COUNTER = False

# Usage function

def usage(status=0):
	print '''usage: uniq.py [-c] files ..."

    -c      prefix lines by the number of occurrence'''.format(os.path.basename(sys.argv[0]))
	sys.exit(status)

# getopts implementation

try:
	options, remainder = getopt.getopt(sys.argv[1:],'c')
except getopt.GetoptError as e:
	print e
	usage(1)

for opt, arg in options:
	if opt == '-c':
		COUNTER = True
	else:
		usage(1)

# read from standard input

if len(sys.argv) == 1:
	sys.argv.append('-')
if COUNTER and len(sys.argv) == 2:
	sys.argv.append('-')

for path in sys.argv[-1]:
	if path == '-':
		stream = sys.stdin
	else:
		stream = open(sys.argv[-1],"r")

# write to outfile

newlines = []
outfile = open('outfile.txt','r+')

repeated_lines = []
for line in stream:
	if line not in newlines:
		newlines.append(line)
		outfile.write(line)
	else:
		repeated_lines.append(line)
outfile.close()
stream.close()

# display results

for line in file('outfile.txt'):
	if COUNTER:
		if line == '\n':
			continue
		occurences=1
		for element in repeated_lines:
			if line != element:
				continue
			elif line == element:
				occurences += 1
				continue
		print occurences, line.strip()
	else:
		print line.strip()

