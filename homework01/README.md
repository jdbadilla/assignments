Exercise 01
-----------

1. Absolute path: cd /afs/nd.edu/user14/csesoft

	The absolute path takes you to the specified directory regardless of the directory in which you are currently in.

2. Relative path: cd ../../user14/csesoft

	The relative path to the directory specified by the path followed from the directery in which you are.

3. Since the tilde represents the home directory, I would use:
	
	cd ~/../../user14/csesoft

4. Go to the home directory and:
	ln -s /afs/nd.edu/user14/csesoft csesoft


Exercise 02
-----------

1. I would use the cp command:

	cp -a /usr/share/pixmaps/. images

2. There are many broken links. In order to see which links were broken, I used:
	
	find . -xtype -l

	(xtype is a test performed on a dereferenced link)

3. To rename the images folder to pixmaps, I would go on my Home folder and type:

	mv images pixmaps

	Upon timing the operation with 'time', it calculated to have taken approximately 0 seconds.

4. To move the pixmaps directory to /tmp/jbadilla-pixmaps, I would type:

	mv pixmaps /tmp/jbadilla-pixmaps

	Upon timing the operation, it calculated to have taken approximately .014 seconds. This operation is slower than just renaming the directory because it requires moving the folder to a completely different directory rather than creating one to move all the contents into and then deleting the original one.

5. To delete the folder, I would type:

	rm -rf /tmp/jbadilla-pixmaps

	Upon timing te operation, it calculated to have taken approximately 0 seconds. This operation is faster than the previous move command. The -rf ensures that the user does not receive prompts regarding deletion approval.


Exercise 03
-----------

1. To list the contents of the software directory such that you get a long listing with file sizes in human readable format, you could type:

	du -h /afs/nd.edu/user37/ccl/software

2. To list the contents of the software directory such that you get a long listing of objects from newest to oldest (newest at the top), I would use:

	ls -lt /afs/nd.edu/user37/ccl/software

3. To determine the number of files in this folder (excluding directories), I used:

	find /afs/nd.edu/user37/ccl/software/cctools/x86_64 -type f | wc -l

	The -type option allows me to narrow the search down to files only, and the wc command allows me to count the number of lines printed. There are a total of 1937 files.

4. In order to determine if the directory contained the file 'weaver', I used:

	find /afs/nd.edu/user37/ccl/software/cctools/x86_64 -executable -type f -name 'weaver'

	The -name option allows me to bring up any file under the directory that is named weaver. The directory indeed contain not one, but two files named weaver. One of them under /afs/nd.edu/user37/ccl/software/cctools/x86_64/redhat6/bin/ and another one under /afs/nd.edu/user37/ccl/software/cctools/x86_64/osx-10.9/bin/.

5. The directory names are redhat5, redhat 6 and osx-10.9. The lines of code I would use are:

	cd /afs/nd.edu/user37/ccl/software/cctools/x86_64	(to navigate to the desired directory)
	du redhat5 redhat6 osx-10.9 -hs

	This shows that redhat5 is the largest directory, with 77 M.

6. From the above directory, use:

	cd redhat5
	find . -type f | wc -l

	There are a total of 768 files.

7. To determine the largest file (in terms of bytes), I used:

	du -a -h | sort -n

	The largest file is called /afs/nd.edu/user37/ccl/software/cctools/x86_64/bin/chirp and is 989 K.

8. To determine which files haven't been modified in more than 30 days, I would use:

	find -mtime +30 | wc -l


Exercise 04
-----------

1. The actual user has permission to read, write and execute the file. The group users have read and execute permissions, but do not have writing permissions. The other users can only execute the file, not read or write it.

2. Original Permissions: rwxr-x--x

	For all the following, I would use the command chmod:

	a. Only you can read and write the file:
		chmod g-r filename
		(This would remove reading writes for the group users)

	b. Only you and members of your group can read, write and execute the file:
		chmod g+wx, o-x filename

	c. Anyone can read the file:
		chmod o+r filename

	d. There are no permissions on the file:
		chmod u-rwx, g-rx, o-x filename

3. If there are no permissions on the file, no one is supposed to be able to delete the file. However, the user can change the permissions and then delete the file.


Exercise 05
-----------

1. The difference between the directories is the permissions set among different users for each one:

For my HOME directory, nd_campus and members of the system:authuser group have lookup (l) permission; members of the system:administrators group have lookup, insert (i), delete (d), administer (a), read (r), write (w) and lock (k) permissions; I have all the Directory and File permissions (rlidwka).

For my Public directory, nd_campus and members of the system:authuser group have read, lookup and lock permissions; members of the system:administrators group and I have all the Directory and File permissions (rlidwka);

For my Private directory, only the members of the system:administrators group and I have all (rlidwka) permissions.

*Directory Permissions*

lookup -- functions as something of a gate keeper for access to the directory and its files

insert -- enables a user to add new files to the directory

delete -- this permission enables a user to remove files and subdirectories from the directory or move them into other directories

administer -- enables a user to change the directory's ACL

*File Permissions*

read -- enables a user to read the contents of files in the directory and to issue the ls -l command

write -- enables a user to modify the contents of files in the directory and to issue the chmod command

lock -- enables a user to run programs that issue system calls to lock files in the directory

2. The Unix permissions for the afs/nd.edu/common directory are read, write and execute permissions for all users. However, upon trying to create a file with touch, I was denied permission. After typing the fs listacl command though, my username was not listed under the Normal Rights, which means that regardless of the Unix permissions, I still do not have write permission.

3. I would use the following line of code:

	fs setacl -clear -dir afs/nd.edu/user31/jbadilla -acl jbadilla all system:instructor rl


Exercise 06
-----------

The permissions for the files created are as follows:

world1.txt has read and write permissions for the actual user, the group users, and other users as well

world2.txt has read and write permissions only for the actual user, but only read permission for the group users and other users

world3.txt has read and write permissions for the user, but only write permission for the group users and other users

They do not have the same permissions because umask sets default read and write permissions for the user depending on the three digit code in front of it. 

By understanding the composition of this code, the programmer can save time by assigning a 0, 2 or 4 to each of the three spots (actual user, group users and other users), which stand for read+write permissions (0), read permission only (2), and write permission only (4) respectively.
