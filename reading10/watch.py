#!/usr/bin/env python

import getopt
import sys
import os 	#used to include usage function
import time

# Global Variables
INTERVAL=2.0

def usage(status=0):
	print '''usage: watch.py [-n INTERVAL] command ...

	-n INTERVAL	Specify update interval (in seconds)'''.format(os.path.basename(sys.argv[0]))
	sys.exit(status)

try:
	options, remainder = getopt.getopt(sys.argv[1:],'n:')
except getopt.GetoptError as e:
	print e
	usage(1)

for opt, arg in options:
	if opt == '-n':
		INTERVAL = arg
	else:
		usage(1)

COMMAND = sys.argv[-1]

while 1:
	try:
		print 'Every %.1fs: %s' % (float(INTERVAL),COMMAND)
		print ""
		os.system(COMMAND)
		time.sleep(float(INTERVAL))
		os.system("clear")
		print ""	
	except KeyboardInterrupt:
		print ""
		sys.exit(0)