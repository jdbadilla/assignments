#!/usr/bin/env python

import getopt
import sys
import os 	#used to include usage function

index=1
newName=""
def usage(status=0):
	print '''usage: imv.py files ...'''.format(os.path.basename(sys.argv[0]))
	sys.exit(status)

os.system('touch tmp.txt')	#creates temporary file
outfile = os.open("tmp.txt",os.O_RDWR)
for arg in sys.argv[1:]:
	os.write(outfile, arg)
	os.write(outfile,"\n")

if os.system('$EDITOR tmp.txt'):
	print 'Failed to use EDITOR to open file'
	sys.exit(1)
stream = open('tmp.txt')
for filename in stream:
	for char in filename:
		if not char.isspace():
			newName += char
	try:
		os.rename(sys.argv[index],newName)
	except OSError as e:
		print e
		sys.exit(0)
	index += 1
	newName = ""

os.system('rm tmp.txt')