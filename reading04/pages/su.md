su
==========

Name
--------

  su - run a command with substitute user and group ID

DESCRIPTION
-----------

  su allows to run commands with a substitute user and group ID.

  When called without arguments, su defaults to running an interactive shell as root.

  For backward compatibility, su defaults to not change the current directory and to only set the environment variables HOME and SHELL (plus USER and LOGNAME if the target user is not root).


OPTIONS
-------

  -c, --command=command
    Pass command to the shell with the -c option.

  -f, --fast
    Pass -f to the shell, which may or may not be useful, depending on the shell.

  -g, --group=group 
    Specify the primary group.  This option is available to the root user only.

  -s, --shell=shell
    Run the specified shell instead of the default.


HISTORY
-------

  This su command was derived from coreutils' su, which was based on an implementation by David MacKenzie.

SUPPORT
---------

Information about the project can be found at:

⟨https://www.kernel.org/pub/linux/utils/util-linux/⟩