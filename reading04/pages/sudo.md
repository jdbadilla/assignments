sudo
==========

Name
--------

  sudo - execute a command as another user

DESCRIPTION
-----------

  sudo allows a permitted user to execute a command as the superuser or another user, as specified by the security policy. The invoking user's real (not effective) user ID is used to determine the user name with which to query the security policy.

  sudo supports a plugin architecture for security policies and input/output logging. Third parties can develop and distribute their own policy and I/O logging plugins to work seamlessly with the sudo front end.


OPTIONS
-------

  -A, --askpass
    Normally, if sudo requires a password, it will read it from the user's terminal.

  -g group, --group=group
    Run the command with the primary group set to group instead of the primary group specified by the target user's password database entry

  -u user, --user=user
    Run the command as a user other than the default target user (usually root)

  -h host, --host=host
    Run the command on the specified host if the security policy plugin supports remote commands


AUTHORS
-------

See the CONTRIBUTORS file in the sudo distribution (https://www.sudo.ws/contributors.html) for an exhaustive list of people who have contributed to sudo.


SUPPORT
---------

Support can be found at:

⟨https://www.sudo.ws/mailman/listinfo/sudo-users⟩