zip
==========

Name
--------

  zip - package and compress (archive) files

DESCRIPTION
-----------

    zip is a compression and file packaging utility for Unix, VMS, MSDOS, OS/2, Windows 9x/NT/XP, Minix, Atari, Macintosh, Amiga, and Acorn RISC OS

    The program is useful for packaging a set of files for distribution; for archiving files; and for saving disk space by temporarily compressing unused files or directories.

COMMAND FORMAT
--------------

    $ zip options archive inpath inpath

OPTIONS
-------

  -ds size
    Determine size

  -f, --freshen
    Replace (freshen) an existing entry in the zip archive only if it has been modified more recently than the version already in the zip archive

  -O output_file
  --output-file output_file
    Process the archive changes as usual, but instead of updating the existing archive, output the new archive to output-file. Useful for updating an archive without changing the existing archive and the input archive must be a different file than the output archive.

  --password "password"
    Use password to encrypt zipfile entries (if any)

  -r
  --recurse-paths
    Travel the directory structure recursively

  -u
  --update
    Replace (update) an existing entry in the zip archive only if it has been modified more recently than the version already in the zip archive

RESOURCES
---------

Information about the project can be found at:

⟨http://linux.die.net/man/1/zip⟩