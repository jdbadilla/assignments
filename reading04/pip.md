pip
==========

Name
--------

  pip - run a command for python packages

DESCRIPTION
-----------

  pip allows you to manage, install and upgrade Python packages


OPTIONS
-------

  install package_Name
    installs specified package

  show --files package_Name
    Show what files were installed for specified package

  list --outdated
    List outdated packages

  install --upgrade package_Name
    Upgrade a package

  uninstall package_Name
    Uninstall a package


SUPPORT
---------

Information about the project can be found at:

⟨https://pip.pypa.io/en/stable/⟩