Reading 04
==========

1. Extracting the contents of 'file.tar.gz':
	
	$ tar xvzf file.tar.gtz

2. Creating an archive named 'data.tar.gz' which consists of the contents of the directory 'data':

	$ tar cvzf data.tar.gz data

3. Extracting the contents of 'file.zip':

	$ unzip file.zip

4. Creating an archive named 'data.zip' which consists of the contents of the directory 'data':

	$ zip -r archive data

5. Installing a package on a Debian based distribution:

	$ apt-get install package_Name

 Instead of using the apt-get command-line tool, Mac uses Homebrew. Installing a package on Mac:

 	$ brew install package_Name

 6. Installing a package on a Red Hat based distribution:

 	$ yum install package_Name

 7. Installing a Python package (on the Mac):

 	$ brew install python 		//installs python
 	$ pip install package_Name	//installs python packages

 8. Pasting the contents of a file to an online pastebin:

 	$ cat file_name | curl -F 'sprunge=<-' http://sprunge.us

 9. Running commands as the root user (on the Mac):

 	$ sudo su
 	Password:
 	$ [enter password]
