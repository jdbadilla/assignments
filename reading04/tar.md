tar
==========

Name
--------

	tar - an archiving utility

DESCRIPTION
-----------

    GNU tar is an archiving program designed to store multiple files in a single file (an archive), and to manipulate such archives.

    For example, the c option requires creating the archive, the v option requests the verbose operation, and the f option takes an argument that sets the name of the archive to operate upon.

OPTIONS
-------

  -r, --append
    Append files to the end of an archive.  Arguments have the same meaning as for -c (--create).

  -x, --extract, --get
    Extract files from an archive.  Arguments are optional.  When given, they specify names of the archive members to be extracted.

  -c, --create
    Create a new archive.  Arguments supply the names of the files to be archived.  Directories are archived recursively, unless the --no-recursion option is given

  -t, --list
    List the contents of an archive.  Arguments are optional. When given, they specify the names of the members to list.

  -d, --diff, --compare
    Find differences between archive and file system.  The arguments are optional and specify archive members to compare.  If not given, the current working directory is assumed.

RESOURCES
---------

Information about the project can be found at:

⟨http://www.gnu.org/software/tar/manual⟩