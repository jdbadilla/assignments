#!/bin/sh
#automatically compiles every source code file in a folder into an executable

: ${VERBOSE:=0}
: ${CFLAGS:="-std=gnu99 -Wall"}
: ${CC:="gcc"}
: ${SUFFIXES:=".c"}


for arguments in *$SUFFIXES; do
	output=$(basename $arguments $SUFFIXES)
	if [ $VERBOSE -eq 1 ]; then
		$CC $CFLAGS -o $output $arguments
		echo $CC $CFLAGS -o $output $arguments
	else 
		$CC $CFLAGS -o $output $arguments
	fi
	[ -e "$output" ] || exit 1
done
