#!/bin/sh

: ${COMMAND:="du -h"}
num=0

while getopts n:a name; do
	case $name in
		n)
			if [ -z OPTARG ]; then
				num=10
			else
				num=$OPTARG
			fi
			;;
		a)
			COMMAND="du -ah"
			;;
	esac
done

shift $(($OPTIND -1))

if [ $num -ne 0 ]; then
	for i in $@; do
		eval argument=\$i
		du -h $argument > output_file.txt 2> /dev/null
		sort -rh output_file.txt > file.txt
		head -n $num file.txt
		rm output_file.txt
	done
else
	$COMMAND $@ > file.txt 2> /dev/null
	sort -rh file.txt
fi
rm file.txt 2> /dev/null