Homework 04
===========

Task 1
------

1.	a) The default environmental variables for compiling were set using parameter expansion at the very beginning of the file. This allowed me to override variables defined within the script. All the overriding takes place inside if statements within a for loop which reads through the current directory and looks for files ending with .c.

	b) Like I said, I used a for loop to iterate over all the files in the current directory. By specifying the SUFFIXES environmental variable at the beginning of the file, I was able to filter out all files that did not end in .c, and thus could not be compiled to form an executable.

	c) The VERBOSE variable is initialized in the parameter expansion at the very top. I used a conditional if statement in the aforementioned for loop to test the condition that once the variable is set to one, it uses echo to emit the command used to compile the executable.

	d) The program is terminated early with the if statement checking for the executable at the very end of the loop. If it fails, the program will stop.

2. Clearly, one of the advantages of using bake.sh is that it compiles every .c file in the current directory, while make only compiles the specified targets within it. However, a makefile is definitely more useful if one wishes to compile only a certain number of files or if one wishes to be more specific with each object being compiled (one might want to specify certain flags to compile a particular object).


Task 2
------

1.	a) In order to parse the command line arguments I used the getopts command, which obtains and their arguments from a list of specified parameters. I used a while loop and a switch statement within it to do this, in which I also specified the argument taken by option -n.

	b) When only the command is run and no other arguments are present, running it will give you the size of the current directory. When no number is passed to the -n option (which specifies how many directories or files will be shown), it defaults to 10. This is done by initializing and using a variable called 'num'.

	c) Instead of handling the directories one by one, I used the du -h command and redirected it to a temporary file which sorts its contents according to size, and outputs this to another file. The head command is then ran on this new file with the specified number of arguments (using the num variable), which determines how many files are to be displayed.

	d) As said before, a temporary file is created for the output redirection of 'du -h; and its contents are then sorted according to size with the 'sort -rh' command. The head command was then used, which serves the convenient purpose of displaying the first n lines of the file or document.

2. The hardest part about this script was learning and implementing the syntax for the case statement and conceptualizing how I was gonna pass the files with the biggest size. Output redirection definitely made writing this script a lot less hard than it had to be. Redirecting the output of the 'du -h' command to a file and working with that file seemed like the easiest way to approach this problem. Defining how the specified number of directories would be read and displayed (the argument for option -n) was definitely what took up the most amount of code. This is not surprising because these are the things that make a command task-specific and it makes sense it would be the most complex part of its script.

Task 3
------

1.	a) I handled the signals by using trap statements, which perform a specificied command whenever a signal is detected whenever the program is run.These are declared at the very beginning of my taunt.sh file.

	b) One must use here documents in order to display long messages, or messages that are more than a line long. In order to create a here document within the shell script, simple append 'EOF' to the command specified, write your message on the following lines and then use EOF again.

	c) Handling the timeout was definitely the toughest part of this task. It took me a while before realizing that one sleep command running in the foreground would not allow the shell script to determine if a trap had been triggered. This is why instead of having one long sleep command, I have 10 'sleep 1' commands so that the trap statement can be checked at the end of each one.

2. It depends on what kind of programming is being done. If the task at hand involves simply detecting a signal, then the shell scripting may be more useful and straightforward. However, if one were to design anything that requires multiple variables, loops and statements, using the C programming language would be better since it contains specific classes for each particular type of variable (i.e. it is easier to keep track of what a program is doing in C). I also personally enjoy the syntax of the C language more (not having to use the dollar sign to denote a variable's value).