USEFUL MISC COMMANDS
====================

*if permission is denied when compiing an executable, write a makefile which compiles it using the .c file and the .o file*

You can use Ctrl+U to clear up to the beginning of the command line!!!

DOWNLOADS
---------

To re-install Xcode developer tools:

	$ xcode-select --install


FILES
-----

* IN ORDER TO HAVE A FILE BE AN EXECUTABLE, COMPILE IT WITH A .c AND A .o file *


To display all (including hidden) files:

	$ ls -a

To display files according to size from greatest to smallest (human readable):
	
	$ ls -lhS

	(otherwise it will display them in alphabetical order)

To display the files from newest to oldest:

	$ ls -lt


To display (human-readable) disk usage of all files:

	$ du -h
	$ du -hc 	//produces a grand total
	$ du -hca	//includes files, not just directories


Pasting the contents of a file to an online pastebin:

 	$ cat file_name | curl -F 'sprunge=<-' http://sprunge.us 	//with sprunge.us
 	(flag -F indicates to output content of file)

Generate random file of specified size (you may use numbers of letters to denote size):

dd if=/dev/urandom of=sample.txt bs=1G count=1


Find file within specified directory within all directories inside it:

find . -name "pattern" -print


	FILE HIERARCHY -- ROOT
	--------------	  ----

		1. Nodes in the tree are files:
			-Regular
			-Directories
			-Links
			-Sockets, Pipes, Devices...

		2. Directories can contain other files

		3. Each directory has a special purpose

		4. Underneath the tree abstraction are objects called 'inodes'

			-inodes: data structures that represent a filesystem object

		/afs --> Distributed network storage
		/bin --> System applications
		/etc --> System configurations
		/lib --> System libraries
		/tmp --> Scratch space
		/usr --> User applications
		/var --> application data


	FILE HIERARCHY -- USER
	--------------    ----

		Every user has a 'home' directory:

			Desktop --> Desktop Folder
			Private --> AFS private folder
			Public  --> AFS public folder
			YESTRDAY--> daily backups 



PROCESSES
---------

Environment Variable: a dynamic "object" on a computer that stores a value that affects the way running processes will behave on a computer

	Environment variables help programs know:
		- what directory to install files in
		- where to store temporary files 
		- where to find user profile settings
		- etc

	The PATH environment variable specifies a set of directories where executable programs are located. To change it, use:

		$ setenv PATH $PATH\:[value]

To find out what the process number identifier of a process is:

	$ pidof process
	OUTPUT: ###

To see information, including inode information for a file:

	$ stat [filename]

	OUTPUT:
	File: `test/file'
  	Size: 41        	Blocks: 2          IO Block: 4096   regular file
	Device: 14h/20d	Inode: 1936277586  Links: 1
	Access: (0700/-rwx------)  Uid: (208080/jbadilla)   Gid: (   40/     dip)
	Access: 2016-02-11 20:24:22.000000000 -0500
	Modify: 2016-02-11 20:24:22.000000001 -0500
	Change: 2016-02-11 20:24:22.000000000 -0500

	• The Inode number is a unique number for each file which is used for the internal maintenance of the system
	• Change: [time] --> last change time of the inode data of the file


PERMISSIONS
-----------

1. The first symbol in directory specifies path:
	d: directory
	-: regular
	l: link
	c: special file
	s: socket
	p: named pipe
	b: block device

2. To set file permissions, you use the 'chmod' command.
	
	One can either use chmod u+rwx,go+rx [file]
	OR
					   chmod 755 [file]

	4 = r = read permission
	2 = w = write permission
	1 = x = execute permission

	u = actual user
	g = group user
	o = other user


LINKS
-----

1. To create links, use 'ln':
	• ln creates hard links by default
	• To create a soft link (or symlink), use flag -s

2. The syntax is as follows:
	ln -s [destination] [link_name]


EXTRACTION
----------

Extracting a tar archive that is the compressed with the gzip utility:

	$ tar xzvf compressed.tar.gz
	(z flag: indicates you wish to use gzip compression on top of the archive
	 x flag: extracts/decompresses)


COMPRESSION
-----------

Create a compresed archive that is compressed with the gzip utility:

	$ tar czvf compressed.tar.gz [directory_name]
	(peek inside with flag t instead of flag c)


Zip the current folder and its subdirectories recursively:

	$ zip -r archive


You can compress files (quickly but not as thougroughly) with gzip by typing a command like this:

	$ gzip sourcefile
	(This will compress the file and change the name to sourcefile.gz on your system)

	OR

	$ gzip -r directory1
	(For recursively compressing directories)

	
You can adjust the compression optimization by passing a numbered flag between 1 and 9:

	The -1 flag (and its alias --fast) represent the fastest, but least thorough compression

	The -9 flag (and its alias --best) represents the slowest and most thorough compression

	-6 is th default


LIBRARIES
---------

Create static library:

ar -cvq libctest.a ctest1.o ctest2.o


List files in library:

ar -t libctest.a


Find out what shareable libraries are linked to an exectuable:

otool -L executable_name


PRIVACY
-------

Run commands as root-user:

	$ sudo su
	

To send out an email and specify the sender from the same local host:

nc localhost 25
MAIL FROM: emailaddress
RCPT TO : otheremail
DATA
Subject: subjectname
body_of_email (type whatever the email is)
.
QUIT


GIT
---

For the git command there are various options:

	$ git init	 //initializes a new repository to track

	$ git clone [ADDRESS]	//creates a copy repository

	$ git status	//views the changes you have made

		-- for simplified status: --

			git status -s

			OUTPUT: (Two columns)

			Left column -> staged
			Right column -> just modified

			?? [file] -- new file that isn't tracked
			A [file] -- added to the staging area
			M [file] -- modified file

	$ git add [FILE/DIRECTORY]	//tracks and stages new file or directory

		*(if 'git add' receives a DIRECTORY, it will copy all its files recursively)*

	$ git commit //commits or saves your changes

		-- git config --

			$ git config --global core.editor [EDITOR] //Changes which text editor opens on default with git commit

			$ git config --list //checks your settings

	$ git remote	//displays the shortname of which remote handle you have specified
		$ git remote -v //displays URLs that Git has stored

	$ git fetch	[remote-name]	//fetches any new work from remote repository but doesn't merge
	$ git pull	//fetches AND merges remote repository to current repository

	$ git push [remote-name] [branch-name]	//pushes or shares work


SIGNALING
---------

To signal a process means to inform programs that some important event has occured.

Signals:
	• TERM (15) --> Terminate the process
	• INT  (2)  --> Interrupt the process
	• KILL (9)  --> Kill the process
	• HUP  (1)  --> Hangup

To run a process in the background:

	$ [command] &	//ampersand means run in background

To see which jobs are running:
	
	$ jobs

To bring job to foreground:
	
	$ fg

	To suspend job:
		
		$ [CTRL-Z]

		To continue running in background after being suspended:

			$ bg

To kill a job:

	$ kill %[job-number]
	OR
	$ kill [PID]


NETWORKING
----------

IP Addres














