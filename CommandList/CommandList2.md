SHELL
=====

	echo
	----

		$ echo $PATHS		//To list the directories that the shell searches in to find executables and other programs:

	export
	------

		$ export PATH=$PATH:[directory]

	alias
	-----

		$ alias today='date +"%A, %B %-d, %Y"'		//to create a command to display or do something (in this case, displays the current date)


	SHELL SCRIPTING
	===============

	echo "Text goes here"		//works like printf (prints endline character)
		echo -n "Text goes here -->"	//works like printf without printing the endline character

	read text 	//works like scanf

	echo "You entered: $text"
		// $() substitutes the results of the enclosed command //


	cat
	---

	Cat to create a file:

		$ cat > groceries.txt
		bananas
		apples
		carrots
		onions
		[CTRL-D]
		$

	or to append:

		$ cat >> groceries.txt
		fruity pebbles
		[CTRL-D]

	or to count lines

		$ cat -n groceries.txt
			1 bananas
			2 apples
			3 carrots
			4 onion
			5 fruity pebbles


	tr
	--

	$ echo "What the fuck" | tr [:lower:] [:upper:]		//turns string into upper case (or viceversa)
	WHAT THE FUCK

	$ echo 'ed, of course!' |tr -d aeiou		//delete characters from a string

	$ echo 'extra     spaces – 5’ | tr -s [:blank:]		//suppress extra white space


	colrm
	-----

	cat grocery.list | colrm 4		//deletes column number of string (the character in that position)


	diff
	----

	compares files (-w and -i to ignore whitespace and case, respectively)

	comm
	----

	shows lines only in file 1, only file 2 and then on both

	fold
	----

	$ fold -w8 file 	//folds file into specified character length per line


	sort
	----

	$ sort -u filename	//sorts contents of file, removing duplicate rows
	$ sort -r filename	//reverses order
	$ sort -n filename	//sort in numeric order



	sed
	---

	sed -- stream editor

	Basic substitution:

		$ echo "IBM 174.99" |sed –e 's/IBM/International Business Machines/g'
		International Business Machines 174.99

		-e = specifies expression or edit script
		s = substitution command
		/ = delimiter
		g = make change globally


	grep
	----

	$ grep 'word' text.txt 		//returns line with word match
	$ grep -c 'word' text.txt 	//returns number of matches
	$ grep -l 'word' text.txt   //returns file name if there's a match
	$ grep -n 'word' text.txt   //includes line number

	egrep --> accepts more arguments than grep

