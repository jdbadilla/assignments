#!/bin/sh

rolls=10
sides=6
counter=0

while getopts "r:s:" opt; do
	case $opt in
		r) if [ -z $OPTARG ]; then
			rolls=10
		   else
		   	rolls=$OPTARG
		   fi
		   ;;
		s) if [ -z $OPTARG ]; then
			sides=10
		   else
		   	sides=$OPTARG
		   fi
		   ;;
	esac
done

shift "$((OPTIND - 1))" ## this dissociates the option arguments from extra independent arguments

while [ $counter -lt $rolls ]; do
	shuf -i 1-"$sides" -n 1
	counter=$((counter+1))
done
