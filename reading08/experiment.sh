#!/bin/sh

rolls=1000

./roll_dice.sh -r $rolls > filename.txt

awk '/^1/ {sum += 1} END {NF = 1} {print $1,"\t",sum}' filename.txt | awk '$1 ~ /^1/' | uniq | tail -1 > results.dat
awk '/^2/ {sum += 1} END {NF = 1} {print $1,"\t",sum}' filename.txt | awk '$1 ~ /^2/' | uniq | tail -1 >> results.dat
awk '/^3/ {sum += 1} END {NF = 1} {print $1,"\t",sum}' filename.txt | awk '$1 ~ /^3/' | uniq | tail -1 >> results.dat
awk '/^4/ {sum += 1} END {NF = 1} {print $1,"\t",sum}' filename.txt | awk '$1 ~ /^4/' | uniq | tail -1 >> results.dat
awk '/^5/ {sum += 1} END {NF = 1} {print $1,"\t",sum}' filename.txt | awk '$1 ~ /^5/' | uniq | tail -1 >> results.dat
awk '/^6/ {sum += 1} END {NF = 1} {print $1,"\t",sum}' filename.txt | awk '$1 ~ /^6/' | uniq | tail -1 >> results.dat

cat results.dat