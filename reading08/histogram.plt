# gnuplot script

set grid
set title 'Dice Roll Results'
set xrange [0:7]
set yrange [0:200]
set xlabel 'side (outcome)'
set ylabel 'frequency'
set style fill solid
plot 'results.dat' with boxes
set term png
set output 'results.png'
replot