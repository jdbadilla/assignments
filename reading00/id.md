TLDR - id
==========

Overview
--------

Print user and group information for the specified USER, or (when USER omitted) for the current user.


Examples
--------

- Print only the security context of the process
	
	$ id -Z

- Print only the effective group ID

	$ id -g

- Print all group IDs
	
	$ id -G

- Print a name instead of a number

	$ id -n

- Print the real ID instead of the effective ID
	
	$ id -r

- Print only the effective user ID
	
	$ id -u


Resources
---------

- Full documentation at: (https://www.gnu.org/software/coreutils/id)
	or available locally via: info '(coreutils) id invocation'
