TLDR - who
==========

Overview
--------

Print information about users and who are currently logged in.


Examples
--------

- Time of last system boot
	
	$ who -b

- Print dead processes

	$ who -d

- Print line of column headings
	
	$ who -H

- Print system login processes

	$ who -l

- Print only hostname and user associated with stdin
	
	$ who -m


Resources
---------

- Full documentation at: (https://www.gnu.org/software/coreutils/who)
	or available locally via: info '(coreutils) who invocation'
