TLDR - git
==========

Overview
--------

[Git] is a distributed version control system.

Examples
--------

- **Clone** a remote *repository*:

        $ git clone git@bitbucket.org:CSE-20189-SP16/assignments.git

- Determine which files are in which state:

	$ git status

- Begin tracking a new file:
	
	$ git add filename

- **Commit** your changes:
	
	$ git commit

- **Push** to your remotes:

	$ git push [remote-name] [branch-name]

Resources
---------

- [Pro Git](https://git-scm.com/book/en/v2)

[git]: https://git-scm.com/
