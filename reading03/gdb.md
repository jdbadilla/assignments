gdb
==========

Name
--------

	gdb - The GNU Debugger

DESCRIPTION
-----------

    The purpose of a debugger such as GDB is to allow you to see what is going on "inside" another program while it executes -- or what another program was doing at the moment it crashed.

    You can run "gdb" with no arguments or options; but the most usual way to start GDB is with one argument or two, specifying an executable program as the argument:

    gdb [program]


OPTIONS
-------

    -h
      List all options, with brief explanations.

	  -s [file]   
      Read symbol table from file file.

    -write  
    	Enable writing into executable and core files.

    -d [directory]
      Add directory to the path to search for source files.

    -cd=[directory]
      Run GDB using directory as its working directory, instead of the current directory.

RESOURCES
---------

Information about the project can be found at:

⟨http://www.gnu.org/software/gdb/⟩