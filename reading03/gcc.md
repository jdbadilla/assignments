gcc
==========

Name
--------

	gcc - GNU project C and C++ compiler

DESCRIPTION
-----------

    When you invoke GCC, it normally does preprocessing, compilation, assembly and linking.  The "overall options" allow you to stop this process at an intermediate stage.

    Most of the command-line options that you can use with GCC are useful for C programs; when an option is only useful with another language (usually C++), the explanation says so explicitly.

OPTIONS
-------

-c  Compile or assemble the source files, but do not link.  The
           linking stage simply is not done.  The ultimate output is in the
           form of an object file for each source file.

           By default, the object file name for a source file is made by
           replacing the suffix .c, .i, .s, etc., with .o.

           Unrecognized input files, not requiring compilation or assembly,
           are ignored.

-S  Stop after the stage of compilation proper; do not assemble.  The
           output is in the form of an assembler code file for each non-
           assembler input file specified.

           By default, the assembler file name for a source file is made by
           replacing the suffix .c, .i, etc., with .s.

           Input files that don't require compilation are ignored.

-E  Stop after the preprocessing stage; do not run the compiler
           proper.  The output is in the form of preprocessed source code,
           which is sent to the standard output.

           (Input files that don't require preprocessing are ignored.)

-o file
           Place output in file file.  This applies to whatever sort of
           output is being produced, whether it be an executable file, an
           object file, an assembler file or preprocessed C code.


-pg 
		   Generate extra code to write profile information suitable for the
           analysis program gprof.  You must use this option when compiling
           the source files you want data about, and you must also use it
           when linking.

-g         Produce debugging information in the operating system's native
           format (stabs, COFF, XCOFF, or DWARF 2).  GDB can work with this
           debugging information.

AUTHORS/CONTRIBUTORS
--------------------

See the info entry for gcc at

	<http://gcc.gnu.org/onlinedocs/gcc/Contributors.html>

...for contributors to gcc.