Reading 03
==========

1. I would use redirection to send the ls output to a file and then use strings to read the contents of the file stored with the information of ls:

	ls > ls.txt
	strings ls.txt

   If actually referring to reading the information within the ls command:

   	strings /bin/ls

2. To determine which libraries ls requires, I would first find its path, navigate there and use the ldd (or otool and option -L on OSX)

	type -p ls
		OUTPUT: ls is /bin/ls

	cd /bin
	otool -L ls
	(OR...
	ldd ls)

3. To view all the system calls made during an invocation of ls:

	strace ls

4. To debug the hello-debug program to fix errors:

	gdb hello-debug

5. To check the hello-dynamic program for memory leaks or errors:

	valgrind hello-dynamic --leak-check=summary

6. To profile the hello-profile program to find any bottlenecks, I would first request parallel profiling by setting the compile flag to -pg, run the program, then the gprof command:

	gcc -pg hello-profile
	a.out
	gprof