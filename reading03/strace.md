strace
==========

Name
--------

  strace - trace system calls and signals

DESCRIPTION
-----------

       In the simplest case strace runs the specified command until it exits.  It intercepts and records the system calls which are called by a process and the signals which are received by a process.

       strace is a useful diagnostic, instructional, and debugging tool. System administrators, diagnosticians and trouble-shooters will find it invaluable for solving problems with programs for which the source is not readily available since they do not need to be recompiled in order to trace them.

OPTIONS
-------

    -c
      Count time, calls, and errors for each system call and report a summary on program exit.

	  -C
      Like -c but also print regular output while processes are running.

    -f
    	Trace child processes as they are created by currently traced processes as a result of the fork(2), vfork(2) and clone(2) system calls

    -q
      Suppress messages about attaching, detaching etc

    -r
      Print a relative timestamp upon entry to each system call.

    -p pid
      Attach to the process with the process ID pid and begin tracing.

    -u username
      Run command with the user ID, group ID, and supplementary groups of username.

HISTORY
-----

  The original strace was written by Paul Kranenburg for SunOS and was inspired by its trace utility.  The SunOS version of strace was ported to Linux and enhanced by Branko Lankester, who also wrote the Linux kernel support.
      
RESOURCES
---------

Information about the project can be found at:

⟨http://sourceforge.net/projects/strace/⟩