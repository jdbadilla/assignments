gprof
==========

Name
--------

	gprof - display call graph profile data

DESCRIPTION
-----------

      The profile data is taken from the call graph profile file (gmon.out default) which is created by programs that are compiled with the -pg option

      If more than one profile file is specified, the "gprof" output shows the sum of the profile information in the given profile files.

      The flat profile shows how much time your program spent in each function, and how many times that function was called.

      The call graph shows, for each function, which functions called it, which other functions it called, and how many times.

      The annotated source listing is a copy of the program's source code, labeled with the number of times each line of the program was executed.

OPTIONS
-------

    -b
      "gprof" doesn't print the verbose blurbs that try to explain the meaning of all of the fields in the tables

	  -L
      Normally, source filenames are printed with the path component suppressed.  The -L option causes "gprof" to print the full pathname of source filenames

    -q
    	The -q option causes "gprof" to print the call graph analysis.

    -a
      The -a option causes "gprof" to suppress the printing of statically declared (private) functions.

FILES
-----

    "a.out"
      the namelist and text space.

    "gmon.out"
      dynamic call graph and profile.

    "gmon.sum"
      summarized dynamic call graph and profile.
      
RESOURCES
---------

Information about the project can be found at:

⟨http://www.gnu.org/software/binutils/⟩