ar
==========

Name
--------

	ar - create, modify, and extract from archives

DESCRIPTION
-----------

	The GNU ar program creates, modifies, and extracts from archives.  An archive is a single file holding a collection of other files in a structure that makes it possible to retrieve the original individual files (called members of the archive).

	The original files' contents, mode (permissions), timestamp, owner, and group are preserved in the archive, and can be restored on extraction.

OPTIONS
-------

   	d   
   		Delete modules from the archive.  Specify the names of modules to be deleted as member...; the archive is untouched if you specify no files to delete.

        (If you specify the v modifier, ar lists each module as it is deleted)

	  m   
      Use this operation to move members in an archive.


    p   
    	Print the specified members of the archive, to the standard output file.  If the v modifier is specified, show the member name before copying its contents to standard output.

        (If you specify no member arguments, all the files in the archive are printed.)

    x
    	Extract members from the archive. You can use the v modifier with this operation, to request that ar list each name as it extracts it.

RESOURCES
---------

Information about the project can be found at:

⟨http://www.gnu.org/software/binutils/⟩