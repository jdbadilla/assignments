#!/bin/sh

usage()
{
	echo "usage: head.sh"
	echo "-n N Display the first N lines"
	exit 1
}

lines=10
while getopts n:h opt; do
	case $opt in
		n) lines=$OPTARG;;
		h) usage;;
	esac
done

shift "$(($OPTIND-1))"

awk -v number_of_lines=$lines 'NR <= number_of_lines'
