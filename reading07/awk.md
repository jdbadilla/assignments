awk
===

One can use the awk command to serve many purposes. To print a line containing a desired string (match a pattern), for instance:

	$ awk '/string/' filename

The awk command modifies input through a file or stdout.

Awk handles input as a set of fields which is separated by a field separator (known as the FS). To print the first field of a string containing whitespace:

	$ echo "this is a string with whispace" | awk END '{print $1}'

		//The above would print 'this' and nothing else.

Awk contains many internal variables. FS, for instance, is the current field separator used to denote each field in a record (by default, it's a whitespace). For instance, to change the current field separator and print out the second element:

	$ awk 'BEGIN { FS = "," } ; { print $2 }'

	Changing the FS (independent of the IFS), is one of the most helpful aspects of using 'awk'

Awk also uses the BEGIN and END containers, which contain specific part of the awk program.

	BEGIN uses:

	•	Declaring variables

	•	Initialization variables for doing increment/decrements operations in main AWK code.

	•	Printing Headings/info before actual AWK code output.


	END uses:

	•	Printing final results, after doing operations in main AWK block.

	•	Printing Completion/info after actual AWK code output.

In an awk array, a[i] holds line length of whole input and i holds the actual string. The input is known as the current record and the number of fields typically refers to the number of words (because they are separated by whitespace). For example, one could store every line starting with 'The' in a file 'filename' by performing the following command:

	$ awk '/^The/ {++a[$0]} END {for(i in a) print i}' filename

There are certain internal variables that store this info, like NF which stores the number of fields in the current record and NR, which the number of the current record (or the current line).

If you wanted to see the first 10 lines of text of a file 'filename', for instance:

	$ awk 'NR <= 10' filename	

	//less than or 10 lines