#!/bin/sh

sum=0
URL="http://catalog.cse.nd.edu:9097/query.text"

if [ $1 ]; then
	URL=$1			##if an argument URL is passed, use that instead
fi

echo -n "Total CPUs: "
curl -s $URL | awk '$1 ~ /^cpu.*/ {sum += $2} END {print sum}'		##prints total number of computers
echo -n "Total Machines: "
curl -s $URL | awk '/^name/ {print $2;}' | uniq | wc -l		##prints total number of machines
echo -n "Most Prolific Type: "
curl -s $URL | sed '/^s*$/d' | awk '/^type/ {++a[$0]} END {for(i in a) if(a[i]>max) {max=a[i];k=i} print k}' | sed 's/^type //g'	##prints out most prolific type

	## a[i] holds line length of whole input
	## i holds the actual string