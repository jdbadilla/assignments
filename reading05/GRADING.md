Reading 05 - Grading
====================

**Score**: 4 / 4

Deductions
----------

Comments
--------

3. mascot.sh
You can also do:

case $(uname) in 
    Linux)
    echo Tux
    ;;
    Darwin)
    echo Hexley
    ;;
    *BSD)
    echo Beastie
    ;;
    *)
    echo Uh... yeah...
    ;;
esac

4. resolve_links.sh
Get all arguments with:
for path in $@; do
...
done
