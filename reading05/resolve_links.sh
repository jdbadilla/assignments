for file in "$1"/*;
do
	if test -L "$file"
	then
		echo -n $file "links to "
		readlink $file
	fi
done