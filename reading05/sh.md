Examples
========

1.) variables:

	# myname=$(whoami)
	# permissions=$(umask)

2.) Capturing STDOUT

	# ls > ls.txt
	# general_file > output

3.) if [ "$string1" == "string2" ]
	then
		echo "The strings are the same."
	else
		echo "The strings are NOT the same."
	fi

4.) case $guitar in
   		"electric") echo "There is an $guitar guitar for sale.";;
 		"acoustic") echo "There is an $guitar guitar for sale.";;
   		"classical") echo "There is a $guitar guitar for sale";;
   		"*") echo "There is no such guitar.";;
	esac

5.) for i in 1 2 3 4 5
	do
		echo "Hello $i times!"
	done

6.) bottle=99

	#continue until no bottles left
	while [ $bottle -gt 1 ]
	do
		echo "$bottle bottles of beer on the wall!"
		n=$(( n-1 ))	# decreases by one each iteration
	done

7.) hello(){
	for n in "$@"; do
		echo "Hello, $n!"
	done
}

8.) trap 'echo "Exit 0 signal detected..."' 0
	echo "This is an example"
    exit 0
