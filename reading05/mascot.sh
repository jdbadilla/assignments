output=$(uname)

if [ "$output" == "Linux" ]
then
	echo "Tux"
elif [ "$output" == "Darwin" ]
then
	echo "Hexley"
elif [ "$output" == "FreeBSD" ] || [ "$output" == "NetBSD"] || [ "$output" == "OpenBSD"]
then
	echo "Beastie"
fi