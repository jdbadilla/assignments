chmod

- allows you to change read write and execute permission rights for a file

- access mode codes
	~ r:read access
	~ w: write permission
	~ x: execute permission

- use-group codes
	~ u: user permissions
	~ g: group permissions
	~ o: permissions for others

- the + and - operators are used to grant or deny a given right to a given group

	chmod u+rwx, go-rwx filename

- FIle Protection

	~ chmod 400 file: To protect a file against accidental overwriting

	~ chmod 500 directory: To protect yourself from accidentally removing, renaming or moving files from this directory

	~ chmod 600 file: private file only changeable by the user who entered this command

	~ chmod 644 file: A publicly readable file that can only be changed by the issuing user

	~ chmod 755 directory: files that should be readable and executable by others, but only changeable by the issuing user

	chmod 775 file: Standard file sharing mode for a group