ps
==========

NAME
----

	ps - report a snapshot of the current processes

DESCRIPTION
-----------

	ps displays information about a selection of the active processes. If you
	want a repetitive update of the selection and the displayed information,
	use top instead.

	This version of ps accepts several kinds of options:

	1.	UNIX options, which may be grouped and must be preceded by a dash.
	2.	BSD options, which may be grouped and must not be used with a dash.
	3. 	GNU long options, which are preceded by two dashes.

EXAMPLES
--------

To see every process on the system using standard syntax:
	ps -e
	ps -ef

To see every process on the system using BSD syntax:
	ps ax
	ps axu

To print a process tree:
	ps -ejH

To get security info:
	ps -eo euser, ruser, suser, fuser, f comm, label
	ps axZ

To see every process running as a root (real & effective ID) in
user format:
	ps -U root

Print only the process IDs of syslogd:
	ps -C

SEE ALSO
--------

	commands: grep, pstree, top, proc

RESOURCES
---------

	Information about the project can be found at

		<https://gitlab.com/procps-ng/procps>
