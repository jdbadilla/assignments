find
==========

Name
--------

	find - allows you to search file names, accept file size, date of last change and other file properties as a criteria for search.

SYNOPSIS
--------

	find [PATH] -command ["STRING"]

DESCRIPTION
--------

	-find [PATH] -name ["STRING"]

		Base of file name (the path with the leading directories removes) matches shell pattern *string*.

	- find [PATH] -path ["SPECIFY PATH"]

		File name matches shell pattern *string*. 

	- find [PATH] -size [(+/-)SPECIFY SIZE]

		File uses n units of space, rounding up. The following suffixes can be used:

			'c'	for bytes
			'w' for two-byte words
			'k' for Kylobytes
			'M' for Megabytes
			'G' for Gigabytes

	- find [PATH] -used [n]

		File was last accessed n days after its status was last changed

	- find [PATH] -links [n]

		File has n links

RESOURCES
--------

		The full documentation for find is maintained as a Texinfo manual. If the info and find programs are properly installed at your site, the command info find should give you access to the complete manual.


