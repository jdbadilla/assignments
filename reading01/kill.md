kill
==========

Name
--------

	kill - terminate a process

DESCRIPTION
--------

	The command kill sends the specified signal to the specified processes
	or process groups. If no signal is specified, the TERM signal is sent.

	Most modern shells have a builtin kill function, with a usage rather
	similar to that of the command described here.


ARGUMENTS
---------

	The list of processes to be signaled can be a mixture of names and pids.

	pid 	Each pid can be one of four things:

			n 	where n is larger than 0. The process with pid n is
				signaled
			0	All processes in the current process group are signaled
			-l 	All processes with a pid larger than 1 are signaled
			-n 	Where n is larger than 1, all processes in process group n are signaled

	names	All processes invoked using this name will be signaled

OPTIONS
-------

	-s, --signal
		The signal to send. It may be given as a name or a number.

	-l, --list
		Print a list of signal names, or convert the given signal number
		to a name

	-a, --all
		Do not restrict the commandname-to-pid conversion to processses
		with the same uid as the present process

RESOURCES
---------

		The kill command is part of the util-linux package and is
		available from Linux Kernel Archive

		<ftp.kernel.com/pub/linux/utils/util-linux>
