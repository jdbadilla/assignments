Reading 01 - Grading
====================

**Score**: 3 / 4

Deductions
----------

-0.5 #4 Need to use du for MyFolder
-0.5 #8 Add setenv EDITOR nano to ~/.cshrc

Comments
--------

#5 Process is not running in the foreground, so need to use kill
