ls
==========

Name
--------

	ls - list directory contents

SYNOPSIS
--------

	ls [OPTION]... [FILE]...

DESCRIPTION
--------

	List information about the FILES (the current directory by default). Sort entries alphabetically if none of -cftuvSUX not --sort is specified.

	Mandatory arguments to long options are mandatory for short options too.

	-a, -all
		do not ignore entries starting with

	-A, -almost-all
		do not list implied . and ..

	-author
		with -l, print the author of each file

	-l
		Display file permissions for the three user categories: Actual user, group owner, other users.

	-i

		Print the index number of each file

	-latr

		Display the files, only in reversed order of the last change, so that the file changed most recently occurs at the bottom of the list

	ls list*

		List all the files in the current dirrectory starting with 'list' (* works as a wildcard)

	ls *list

		List all the files in the current directory ending with 'list'

	ls ?list

		Match all files with exactly one character in the position of the '?' wildcard

RESOURCES
--------

		Full documentation at: 

		<http://www.gnu.org/software/coreutils/ls>

		or available locally via: 
			
		info '(coreutils) ls invocation'
