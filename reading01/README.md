Reading 01
==========

1. I would use the 'chmod' command, which would allow me to change the
reading, writing, and executing permission rights of the file. Namely,
the command I would use is:

	chmod u+r, go-r filename

2. I would go to my home directory and use the ln command. The line of
code I would use, with link name 'link', is:

	ln -s /afs/nd.edu/coursesp.16/cse/cse20189.01 link

3. An easy way to determine what the size of a file is to use the ls
command along with -l:

	ls -l BigFile

4. Likewise, one can determine the size of a directory with ls -ldir:

	ls -ldir MyFolder

5. I would use the command Ctrl+C to terminate and quit this process
running in the foreground.

6. To kill every instance of a certain process, I would use the killall
command with the -KILL option:
	killall -KILL urxvt

7. To determine the time it takes for a program to run we use the 
'time' command:

	time simulation

8. To set the shell's default EDITOR to nano, for instance, I would use:

	git config --global core.editor nano


