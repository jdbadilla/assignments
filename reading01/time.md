time
==========

NAME
----

	time - time a simple command or give resource usage

SYNOPSYS
--------

	time [OPTIONS] command [ARGUMENTS...]

DESCRIPTION
-----------

	The time command runs the specified program command with the given arguments.
	When 'command' finishes, time writes a message to standard error giving
	timing statistics about this program run. 

	These statistics consist of (i) the elapsed real time between invocation
	and termination, (ii) the user CPU time, and (iii) the system CPU time

GNU VERSION
-----------

	When the -p option is given the (portable) output format

		real %e
		user %U
		sys  %S

	is used.


RESOURCES
---------

	A description of the project, info about reporting bugs, and the
	latest version of this page, can be found at:

		<http://www.kernel.org/doc/man-pages>
