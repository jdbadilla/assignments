file
==========

Name
--------

	file - determine file type

SYNOPSIS
--------

	file [OBJECT]...

DESCRIPTION
--------

	file tests each argument in an attempt to classify it. There are three
	sets of tests, performed in this order: filesystem tests, magic tests,
	and language tests. The first test that succeeds causes the file type
	to be printed.


OPTIONS
--------

	--apple
		
		Causes the file command to output the file type and creator
             code as used by older MacOS versions. The code consists of
             eight letters, the first describing the file type, the latter
             the creator.

	-z, --uncompress

		Try to look inside compressed files

	-b, --brief

		Do not prepend filenames to output lines

	-k, --keep-going

		Do not stop at the first match.

	-r, --raw

		Do not translate unprintable chars to \ooo

	-D, --debug

		Print debugging messages

	--help

		Display this help and exit

	-v, --version

		Output version information and exit

EXAMPLES
--------

	$ file reading00/git.md
	reading00/git.md: ASCII text

HISTORY
-------

There has been a file command in every UNIX since at least Research Version 4
(man page dated November, 1973). The System V version introduced one significant
major change: the external list of magic types. This slowed the program down
slightly but made it a lot more flexible.

RESOURCES
---------

		You can obtain the original author's latest version by anonymour FTP on:

		<ftp.astron.com/pub/file/file-X.YZ.tar.gz>


