tr
==

Description
-----------

	'tr' translates or deletes characters

	It translates, squeezes and or deletes characters from standard input, writing to standard output


Syntax
------

	tr [:SOURCE_SET:] [:TARGET_SET:]

Sets
----

	SETs are specified as strings of characters. Some sequences are:

	   [:alnum:]
              all letters and digits

       [:alpha:]
              all letters

       [:blank:]
              all horizontal whitespace

       [:cntrl:]
              all control characters

       [:digit:]
              all digits

       [:graph:]
              all printable characters, not including space

       [:lower:]
              all lower case letters

       [:print:]
              all printable characters, including space

       [:punct:]
              all punctuation characters

       [:space:]
              all horizontal or vertical whitespace

       [:upper:]
              all upper case letters

       [:xdigit:]
              all hexadecimal digits

       [=CHAR=]
              all characters which are equivalent to CHAR

       \t     horizontal tab
       \n     new line
       \\	  backslash
