uniq
====

Description
-----------

	'uniq' reports and/or omits repeated lines

	It is strongly recommended to use flags with this command, as they provide important information about the document being inspected with uniq

Options
-------

	-c, --count
       	label lines with the number of occurrences

    -d
    	print duplicate lines only

    -i
    	ignore case

    -u
    	only print unique lines


Authors
-------

	Richard M. Stallman and David MacKenzie


See Also
--------

Full documentation at: <http://www.gnu.org/software/coreutils/uniq>
