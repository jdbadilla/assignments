Reading 06
==========

1. Convert all the input text to upper case:

	$ echo "All your base are belong to us" | tr [:lower:] [:upper:]
	ALL YOUR BASE ARE BELONG TO US

2. Parse the /etc/passwd file for the shell of the root user:

	$ cat /etc/passwd | grep 'root' | head -n 1 | sed -e 's/^root.*root://'
	/bin/bash

3. Find and replace all instances of monkeys to gorillaz:

	$ echo "monkeys love bananas" | sed -e 's/monkeys/gorillaz/g'
	gorillaz love bananas

4. Find and replace all instances of /bin/bash, /bin/csh, and /bin/tcsh to /usr/bin/python in /etc/passwd:

	$ cat /etc/passwd | sed -e 's/\/bin\/.*/\/usr\/bin\/python/' | grep python

5. Remove any leading whitespace from a string of text:

	$ echo "      monkeys love bananas" | sed -e 's/\s*//'
	monkeys love bananas

6. Find all the records in /etc/passwd that have a field that begins with a 4 and ends with a 7:

	$ cat /etc/passwd | grep '4[0-9]*7'
	rtkit:x:499:497:RealtimeKit:/proc:/sbin/nologin
	qpidd:x:497:495:Owner of Qpidd Daemons:/var/lib/qpidd:/sbin/nologin
	uuidd:x:495:487:UUID generator helper daemon:/var/lib/libuuid:/sbin/nologin
	mailnull:x:47:47::/var/spool/mqueue:/sbin/nologin

7. Follow the trailing output of a set of log files.

	$ tail -f filename

8. Given two files, show all the lines the are present in both files.

	$ comm filename1 filename2

	//displays the lines exclusive to file 1, exclusive to file 2, and pertinent to both