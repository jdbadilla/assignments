sort
====

Description
-----------

	sort - arranges lines of text files

	'sort' writes sorted concatenation of all specified files to standard output

	with no file specified, reads standard input

Use
---

	$ sort filename		//alphabetizes in alphabetical order

	$ sort -u filename	//sorts contents of file, removing duplicate rows

	$ sort -r filename	//reverses order

	$ sort -n filename	//sort in numeric order


See Also
--------

	Full documentation at: <http://www.gnu.org/software/coreutils/sort>