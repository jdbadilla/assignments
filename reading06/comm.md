comm
====

Description
-----------

	'comm' compares to sorted files line by line

	When two file arguments are not given, reads standard input

Options
-------

    -1     suppress column 1 (lines unique to FILE1)

    -2     suppress column 2 (lines unique to FILE2)

    -3     suppress column 3 (lines that appear in both files)

    --check-order
        checks that the input is correctly sorted

See Also
--------

	Full documentation at: <http://www.gnu.org/software/coreutils/comm>