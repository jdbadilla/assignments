sed
===

Description
-----------

	'sed' is a stream editor for filtering and transforming standard input
	
	'sed' is particularly helpful because it can filter text in a pipeline which distinguishes it as one of the most widely used stream editors


Command Sypnosis
----------------

	a \
		append text

	i \
		insert text

	r filename
		append text from file 'filename'

	/d
		delete pattern space (start next cycle --> next line, space, tab)

	s/regexp/replacement/
		substitute matches of regexp for replacement value

 	/w filename
 		write the current pattern space to file 'filename'

See Also
--------

	Full documentation at ⟨http://www.gnu.org/software/sed/⟩





