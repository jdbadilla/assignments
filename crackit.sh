#!/bin/sh

BLACKBOX=~pbui/pub/bin/BLACKBOX

for argument in `strings $BLACKBOX`; do
	if $BLACKBOX -p $argument | grep "granted" > /dev/null; then
		echo the password is $argument
		exit
	fi
done

strings $BLACKBOX | while read argument; do
	if $BLACKBOX -p $argument | grep granted > /dev/null; then
		echo The password is argument
		exit
	fi
done
