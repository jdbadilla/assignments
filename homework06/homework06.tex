\documentclass[letterpaper]{article}
\usepackage[utf8]{inputenc}


\title{Homework 06}
\date{Friday, March 18, 2016}
\author{Jose Badilla \\ email: \href{mailto:jbadilla@nd.edu}{jbadilla@nd.edu}}

\usepackage{graphicx}
\usepackage{hyperref}
\usepackage[margin=1in]{geometry}

\begin{document}

\maketitle

%--------------------------------------------------------

\section*{Overview}

This homework dealt with obtaining information from the provided 'demographics.csv' file using the parsing and editing commands we have learned in class (awk, sed, cut, etc). The information in the .csv file was given in the form of capital letters denoting the gender and then the ethnicity of the student. Upon obtaining the frequency of the genders and ethnicities, I outputted the results to the gender and ethnicity result files respectively. In doing so, I formatted the data and wrote a gnuplot script in order to translate these results into graphical representations of themselves.

From this, I discovered that, in terms of diversity, the recent computer science classes are quite unevenly distributed. Results show that most non-white ethnic groups are hugely underrepresented, and that there is a significant proportion of male over female students. 

%--------------------------------------------------------

\section*{Methodology}

\subsection*{Processing the Data}

This part required previous knowledge of parsing and editing commands to obtain only the desired information from the 'demographics.csv' file. I begin processing the information by creating the files in which the data will go. A piped command line was written to obtain each piece of data. The delimiter -d option in the 'cut' command made it very easy to only print the desired columns:

\begin{verbatim}
    cut -d , -f 1
\end{verbatim}

Because many blank lines were still present and appeared as entries of the first column, I used the sed command with the delete option to get rid of these empty lines:

\begin{verbatim}
    sed '/^\s*$/d'
\end{verbatim}

To keep track of the amount of male and female students, I used an if statement within the awk command, which added 1 to the total count of males and females. Then I printed the results with the respective year:

\begin{verbatim}
    awk '{ if ($1 =="M") 
                {sumM += 1}
           else if ($1 == "F")
                {sumF += 1}
            }
            {print "2013", sumM, sumF}'
\end{verbatim}

To obtain the total number of students per ethnic group, I used a similar piped command line, but I used 'sed' to scan for the desired letter instead. The 'grep' command was also used with the -o flag to count every instance of the letter, even if it appears twice or more on the same line:

\begin{verbatim}
    grep -Eo 'B'
\end{verbatim}

Redirection was used to output all this data and the total occurrence of each letter into a target file.

\subsection*{Producing the Graphs}

Writing the code to produce the graphs with gnuplot script was definitely the most challenging part of this assignment.

Using the two data files containing gender and ethnicity information created upon processing the data, the gnuplot script titled \verb|histogram.plt| serves a twofold purpose.

First, it creates a graph that uses the ethnicities in the \verb|ethnic_results.dat| file to label the categories. It then lists the frequency of each along the y axis, which is the data found in the second column. Because the datafile separator is automatically set to a whitespace, and some ethinicity names are separated by a whitespace (Native American), I made the datafile separator the indentation character:

\begin{verbatim}
    set datafile separator "\t"
\end{verbatim}

In order to give the x axis the categorical name of the ethnicities, the plot command used differed from usual. Also, I rotated these labels so that they wouldn't look convoluted on the graph if printed horizontally:

\begin{verbatim}
    plot 'ethnic_results.dat' using 2:xticlabel(1) title col
    set xtics rotate out
\end{verbatim}

To output the graphs into .png format, I simply included:

\begin{verbatim}
    set term png
    set output 'ethnicity.png'
\end{verbatim}

Second, I created a graph for the proportion of gender among the last five years. Because the ethnicity graph had already been created using the same gnuplot script, the 'reset' command was used before setting the parameters for this new graph.

The x-axis was labeled according to year, and these categorical 'xtics' labels were set by establishing the lower bound, the term of increment, and the upper bound:

\begin{verbatim}
    set xtics 2012,1,2019
\end{verbatim}

Finally, my plot command consisted of shifting the male results to the left and the female results to the right such that they could both appear on top of their respective year label:

\begin{verbatim}
    plot 'gender_results.dat' using($1-.21):2 title "Males" with boxes, \
 	     'gender_results.dat' using($1+.21):3 title "Females" with boxes
\end{verbatim}

%--------------------------------------------------------

\section*{Analysis}

The following tables and figures summarize the 'demographics.csv' data:

\begin{table}[h!]
	\centering
	\begin{tabular}{y||m||f}
	Year	& Males & Females\\
	\hline
	2013	& 49    & 14\\
	2014 	& 44    & 12\\
	2015	& 58    & 16\\
	2016	& 60    & 19\\
	2017	& 65    & 26\\
	2018	& 90    & 36\\
	\end{tabular}
	\caption{Gender Distribution}
	\label{tbl:gendertable}
\end{table}

\begin{table}[h!]
	\centering
	\begin{tabular}{e||s}
	Race	& Students\\
	\hline
	African American    & 18\\
	Native American 	& 19\\
	Oriental            & 50\\
	Caucasian	        & 337\\
	Hispanic	        & 45\\
	Multiple Selection  & 36\\
	Undeclared          & 2\\
	\end{tabular}
	\caption{Racial Distribution}
	\label{tbl:ethnictable}
\end{table}

\begin{figure}[h!]
\centering
\includegraphics[width=5in]{gender.png}
\caption{Gender Distribution}
\label{fig:genderfigure}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=5in]{ethnicity.png}
\caption{Gender Distribution}
\label{fig:genderfigure}
\end{figure}

There are two main things to take away from these results.

    1. Clearly, gender among computer science is unevenly distributed. The distribution proves that there is a significantly larger number of men than women in computer science.
    
    2. There is an uneven distribution of race among computer science. Particularly, there are more Caucasian students in computer science than any other race. In this particular example, it shows that there are more Caucasians than every other race put together, which is pretty alarming.
    
%--------------------------------------------------------

\section*{Discussion}

As an international hispanic who has lived in 4 different continents, diversity is hugely important to me. Aside from providing people with different cultures and insights, diversity invites the majority representation to be respectful and mindful of the minority's customs and ideas. Why, even those of the same race that speak different languages might behave and process differently.

I say this primarily because organizing your thoughts is hugely important in computer science. When programming, one must build small and think big. It's along the way to the final result that ideas differ and code varies. Likewise, people from different cultures will likely share the same goals, but they'll just approach them differently.

Diversity isn't something that is meant to be "trophied", and one shouldn't preach diversity just to have a group of multiracial friends. Diversity shows discrepancies among customs, allows one to reflect on one's own culture, and helps filter the desired behaviours and ideas while disregarding the negative. Much like diversity, coding encourages different ways of thinking to optimize oneself (or own's own code).

With technology meeting the needs of others, now more than ever, having diversity in the field is important because it allows one to see different needs from different points of view. If technology is the solution to our problems, then those working with technology need a global viewpoint to come up with global solutions. It's that simple.

The Computer Science and Engineering program at the University of Notre Dame is very welcoming and unique in terms of student-professor interaction. However, I believe that the student body should be composed of students with more ethnic backgrounds and more women as well.

The University could perhaps better support its students by holding a few mandatory meetings a year regarding the importance of diversity and the ways diversity help an individual grow.

\end{document}