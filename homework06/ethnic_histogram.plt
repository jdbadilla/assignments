# results for Computer Science demographic

set style data histogram
set style histogram clust gap 1
set grid
set style fill solid border #rgb "black"
set auto x
set xlabel 'Ethnicities'
set ylabel 'Frequency'
set yrange [0:*]
set datafile separator "\t"
set boxwidth 1
plot 'ethnic_results.dat' using 2:xticlabel(1) title col
set xtics rotate out
set term png
set output 'ethnicity.png'
replot