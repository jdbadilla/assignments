reset
set terminal png
set yrange [0:100]
set xrange [2012:2019]
set grid
set style data histogram
set style fill solid border
set boxwidth 0.40
set xtics 2012,1,2019
set xlabel 'Year'
set ylabel 'Frequency'
set output 'gender.png'
plot 'gender_results.dat' using($1-.21):2 title "Males" with boxes, \
 	 'gender_results.dat' using($1+.21):3 title "Females" with boxes