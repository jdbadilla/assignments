#!/bin/sh

echo "Gender Results:" > gender_results.dat
cat demographics.csv | cut -d , -f 1 | sed '/^\s*$/d' | awk '{if ($1 =="M") {sumM += 1} else if ($1 == "F") {sumF += 1}} {print "2013", sumM, sumF}' | tail -1 >> gender_results.dat #prints year, male and female in CS
cat demographics.csv | cut -d , -f 3 | sed '/^\s*$/d' | awk '{if ($1 =="M") {sumM += 1} else if ($1 == "F") {sumF += 1}} {print "2014", sumM, sumF}' | tail -1 >> gender_results.dat
cat demographics.csv | cut -d , -f 5 | sed '/^\s*$/d' | awk '{if ($1 =="M") {sumM += 1} else if ($1 == "F") {sumF += 1}} {print "2015", sumM, sumF}' | tail -1 >> gender_results.dat
cat demographics.csv | cut -d , -f 7 | sed '/^\s*$/d' | awk '{if ($1 =="M") {sumM += 1} else if ($1 == "F") {sumF += 1}} {print "2016", sumM, sumF}' | tail -1 >> gender_results.dat
cat demographics.csv | cut -d , -f 9 | sed '/^\s*$/d' | awk '{if ($1 =="M") {sumM += 1} else if ($1 == "F") {sumF += 1}} {print "2017", sumM, sumF}' | tail -1 >> gender_results.dat
cat demographics.csv | cut -d , -f 11 | sed '/^\s*$/d' | awk '{if ($1 =="M") {sumM += 1} else if ($1 == "F") {sumF += 1}} {print "2018", sumM, sumF}' | tail -1 >> gender_results.dat

echo "Ethnic Results:" > ethnic_results.dat
sed 's/[^B]//g' demographics.csv | sed '/^\s*$/d' | grep -Eo 'B' | awk '/B/ {sum += 1} END {print "African American","\t",sum}' >> ethnic_results.dat
sed 's/[^N]//g' demographics.csv | sed '/^\s*$/d' | grep -Eo 'N' | awk '/N/ {sum += 1} END {print "Native American","\t",sum}' >> ethnic_results.dat
sed 's/[^O]//g' demographics.csv | sed '/^\s*$/d' | grep -Eo 'O' | awk '/O/ {sum += 1} END {print "Oriental","\t",sum}' >> ethnic_results.dat
sed 's/[^C]//g' demographics.csv | sed '/^\s*$/d' | grep -Eo 'C' | awk '/C/ {sum += 1} END {print "Caucasian","\t",sum}' >> ethnic_results.dat
sed 's/[^S]//g' demographics.csv | sed '/^\s*$/d' | grep -Eo 'S' | awk '/S/ {sum += 1} END {print "Hispanic","\t",sum}' >> ethnic_results.dat
sed 's/[^T]//g' demographics.csv | sed '/^\s*$/d' | grep -Eo 'T' | awk '/T/ {sum += 1} END {print "Multiple Selection","\t",sum}' >> ethnic_results.dat
sed 's/[^U]//g' demographics.csv | sed '/^\s*$/d' | grep -Eo 'U' | awk '/U/ {sum += 1} END {print "Undeclared","\t",sum}' >> ethnic_results.dat

cat gender_results.dat
echo
cat ethnic_results.dat